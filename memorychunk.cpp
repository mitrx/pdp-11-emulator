#include "memorychunk.h"

MemoryChunk::MemoryChunk(int16_t address, std::array<int8_t, 16> memory)
    : m_address(address), m_memory(memory) { }

MemoryChunk::MemoryChunk(const MemoryChunk& c)
    : m_address(c.m_address), m_memory(c.m_memory)
{
}

MemoryChunk& MemoryChunk::operator=(const MemoryChunk& c)
{
    m_address = c.m_address;
    m_memory = c.m_memory;
    return *this;
}

MemoryChunk::MemoryChunk(MemoryChunk&& c)
    : m_address(c.m_address), m_memory(c.m_memory)
{
}

MemoryChunk& MemoryChunk::operator=(MemoryChunk&& c)
{
    m_address = c.m_address;
    m_memory = c.m_memory;
    return *this;
}

int16_t MemoryChunk::address() const
{
    return m_address;
}

const std::array<int8_t, 16>& MemoryChunk::memory() const
{
    return m_memory;
}
