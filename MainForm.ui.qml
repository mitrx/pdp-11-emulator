import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.1

Item {
    property alias buttonRun: buttonRun
    property alias buttonReset: buttonReset
    property alias buttonStep: buttonStep
    property alias disasmText: disasmText
    property alias regsText: regsText
    property alias hexvText: hexViewerText
    property alias monitorImage: monitorImage
    width: 630
    height: 500

    GroupBox {
        id: hexViewerGroupBox
        x: 8
        y: 300
        width: 614
        height: 192
        spacing: 0
        title: qsTr("Hex Viewer")

        TextArea {
            id: hexViewerText
            font.family: "Courier"
            font.pointSize: 15
            anchors.centerIn: parent
            width: parent.width + 20
            height: parent.height + 20
        }
    }

    GroupBox {
        id: regsGroupBox
        x: 270
        y: 8
        width: 169
        height: 200
        spacing: 0
        title: qsTr("Registers")

        TextArea {
            id: regsText
            anchors.rightMargin: -8
            anchors.leftMargin: -8
            anchors.bottomMargin: -8
            anchors.topMargin: -8
            font.pointSize: 15
            font.family: "Courier"
            anchors.fill: parent
        }
    }

    GroupBox {
        id: disasmGroupBox
        x: 445
        y: 8
        width: 177
        height: 200
        spacing: 0
        title: qsTr("Disassembler")

        Layout.minimumWidth: 150
        Layout.minimumHeight: 180

        Layout.preferredWidth: 180
        Layout.preferredHeight: 200

        Layout.maximumWidth: 250
        Layout.maximumHeight: 250

        TextArea {
            id: disasmText
            anchors.rightMargin: -8
            anchors.leftMargin: -8
            anchors.bottomMargin: -8
            anchors.topMargin: -8
            font.family: "Courier"
            font.pointSize: 15
            anchors.fill: parent
            wrapMode: "WrapAnywhere"
        }
    }

    Button {
        id: buttonRun
        x: 272
        y: 224
        text: qsTr("Run")
    }

    Button {
        id: buttonStep
        x: 399
        y: 224
        text: qsTr("Step")
    }

    Button {
        id: buttonReset
        x: 522
        y: 224
        text: qsTr("Reset")
    }

    Rectangle {
        id: monitorFrame
        x: 8
        y: 24
        width: 258
        height: 258
        color: "#000000"

        Image {
            id: monitorImage
            x: 0
            y: 0
            anchors.centerIn: parent
            width: 256
            height: 256
        }
    }
}
