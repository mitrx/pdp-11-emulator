#ifndef ENCODER_H
#define ENCODER_H

#include "config.h"
#include "instruction.h"

int16_t encodeGenRegAddrMode(const AddressingMode mode);
int16_t encodeProgCountAddrMode(const AddressingMode mode);
int16_t encodeStackAddrMode(const AddressingMode mode);
int16_t encodeAddrMode(const InstructionOperand operand);
int16_t encodeDoubleOperandInstr(const Instruction instr);
int16_t encodeDoubleOperandRegInstr(const Instruction instr);
int16_t encodeSingleOperandInstr(const Instruction instr);
int16_t encodeOneInstruction(const Instruction instr);
int16_t *pdpEncode(const Instruction *instructionList, int count);
#endif // ENCODER_H
