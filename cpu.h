#ifndef CPU_H
#define CPU_H

#include "config.h"
#include "memory.h"
#include "registers.h"
#include "pipeline.h"
#include "alu.h"
#include <stack>

class SlowCpuException: public std::exception
{
  virtual const char* what() const throw()
  {
    return "CPU cycle durating is longer, than it must be";
  }
};

class Cpu
{
public:
    Cpu(Memory &memory);
    ~Cpu();
    void initialize();
    bool cycle();
    size_t cycles();
    int frequency();
    void reset();
    Registers& registers();
private:
    Memory& m_memory;
    Registers *m_registers;
    Pipeline *m_pipeline;
    size_t m_cycles;
    Alu *m_alu;
};

#endif // CPU_H
