#include "registersview.h"
#include <string>
#include <sstream>
#include <iostream>


RegistersView::RegistersView(Emulator& emulator, QObject *parent)
    : QObject(parent), m_registers(emulator.cpu().registers())
{
    m_text = new QString();
}

RegistersView::~RegistersView()
{
    delete m_text;
}

QString RegistersView::text()
{
    return *m_text;
}

void RegistersView::render()
{
    std::stringstream sstream;
    sstream << "R0:" << "  " << m_registers.r0() << std::endl;
    sstream << "R1:" << "  " << m_registers.r1() << std::endl;
    sstream << "R2:" << "  " << m_registers.r2() << std::endl;
    sstream << "R3:" << "  " << m_registers.r3() << std::endl;
    sstream << "R4:" << "  " << m_registers.r4() << std::endl;
    sstream << "R5:" << "  " << m_registers.r5() << std::endl;
    sstream << "SP:" << "  " << m_registers.sp() << std::endl;
    sstream << "PC:" << "  " << m_registers.pc() << std::endl;
    sstream << "PSW:" << " " << m_registers.psw() << std::endl;

    QString *newTextPtr = new QString(QString::fromStdString(sstream.str()));
    if (*newTextPtr != *m_text) {
        delete m_text;
        m_text = newTextPtr;
        emit textChanged();
    }
}
