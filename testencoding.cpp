#include "testencoding.h"

// [DO] double operand instructions
Instruction *createDO_Instr(int16_t addr, InstructionType type,
    AddressingMode firstAddrMode, int16_t firstReg, AddressingMode secAddrMode,
    int16_t secReg, int16_t offset, int comp)
{
    InstructionOperand *firstOp = new InstructionOperand;
    InstructionOperand *secondOp = new InstructionOperand;
    firstOp->setMode(firstAddrMode);
    firstOp->setReg(firstReg);
    secondOp->setMode(secAddrMode);
    secondOp->setReg(secReg);
    Instruction *instr = new Instruction(addr, type, firstOp, secondOp, offset, comp);
//    Instruction instr = new Instruction();
    return instr;
}

// [DOR] double operand register instructions
Instruction *createDOR_Instr(int16_t addr, InstructionType type,
    int16_t firstReg, AddressingMode secAddrMode,
    int16_t secReg, int16_t offset, int comp)
{
    InstructionOperand *firstOp = new InstructionOperand;
    InstructionOperand *secondOp = new InstructionOperand;
    firstOp->setMode(AddressingMode::GR_Register);
    firstOp->setReg(firstReg);
    secondOp->setMode(secAddrMode);
    secondOp->setReg(secReg);
    Instruction *instr = new Instruction(addr, type, firstOp, secondOp, offset, comp);
    return instr;
}
// [SO] single operand instructions
Instruction *createSO_Instr(int16_t addr, InstructionType type,
                           AddressingMode firstAddrMode, int16_t firstReg,
                           int16_t offset, int comp)
{
    InstructionOperand *firstOp = new InstructionOperand;
    firstOp->setMode(firstAddrMode);
    firstOp->setReg(firstReg);
    Instruction *instr = new Instruction(addr, type, firstOp, nullptr, offset, comp);
    return instr;
}

// [NO] no operand instructions
Instruction *createNO_Instr(int16_t addr, InstructionType type, int16_t offset,
                           int comp)
{
    Instruction *instr = new Instruction(addr, type, nullptr, nullptr, offset, comp);
    return instr;
}

/**
MOV R1 R0    | 0010100
ADD R3 R2    | 0060302
MUL R4 (R2)+ | 0070422
XOR R5 -(R0) | 0074540
CLRB (R3)+   | 0105023
DEC R2       | 0005302
ROLB -(R6)   | 0106146
ASLB  R7     | 0106327
WAIT         | 0000001
CLN          | 0000250
 */

Instruction *createInstrList()
{
    Instruction *instrList = new Instruction[N];
    instrList[0] = *createDO_Instr(0, InstructionType::Mov, AddressingMode::GR_Register, R1, AddressingMode::GR_Register, R0, 0, 0);
    instrList[1] = *createDO_Instr(0, InstructionType::Add, AddressingMode::GR_Register, R3, AddressingMode::GR_Register, R2, 0, 0);
    instrList[2] = *createDOR_Instr(0, InstructionType::Mul, R4, AddressingMode::GR_Increment, R2, 0, 0);
    instrList[3] = *createDOR_Instr(0, InstructionType::Exor, R5, AddressingMode::GR_Decrement, R0, 0, 0);
    instrList[4] = *createSO_Instr(0, InstructionType::Clrb, AddressingMode::GR_Increment, R3, 0, 0);
    instrList[5] = *createSO_Instr(0, InstructionType::Dec, AddressingMode::GR_Register, R2, 0, 0);
    instrList[6] = *createSO_Instr(0, InstructionType::Rolb, AddressingMode::ST_Decrement, R6, 0, 0);
    instrList[7] = *createSO_Instr(0, InstructionType::Aslb, AddressingMode::PC_Immediate, R7, 0, 0);
    instrList[8] = *createNO_Instr(0, InstructionType::Wait, 0, 0);
    instrList[9] = *createNO_Instr(0, InstructionType::Cln, 0, 0);
    return instrList;
}

void testEncoding()
{
    Instruction *instructions = createInstrList();
    int16_t *t = pdpEncode(instructions, N);
    for (int i = 0; i < N; i++) {
#ifdef __TESTENCODING_DEBUG__
        std::cout << instructions[i] << " = " << std::oct << t[i] << std::endl;;
#endif
    }
    delete[] instructions;
    delete[] t;
}
