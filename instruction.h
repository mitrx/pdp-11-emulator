#ifndef INSTRUCTION_H
#define INSTRUCTION_H

#include "config.h"
#include <cstdint>
#include <ostream>
#include <istream>
#include <string>


#define MASK_BW ((int16_t) 0100000) // Byte or word

//[DO] double-operand instruction opcodes
//                         0bBxxx000000000000
#define OP_MOV  ((int16_t) 0010000) // Move
#define OP_MOVB ((int16_t) 0110000)
#define OP_CMP  ((int16_t) 0020000) // Compare
#define OP_CMPB ((int16_t) 0120000)
#define OP_BIT  ((int16_t) 0030000) // Bit Test
#define OP_BITB ((int16_t) 0130000)
#define OP_BIC  ((int16_t) 0040000) // Bit Clear
#define OP_BICB ((int16_t) 0140000)
#define OP_BIS  ((int16_t) 0050000) // Bit Set
#define OP_BISB ((int16_t) 0150000)
#define OP_ADD  ((int16_t) 0060000) // Add
#define OP_SUB  ((int16_t) 0160000) // Subtract
//                       0b0^^^000000000000
#define MASK_OPCODE_DOI ((int16_t) 0b0111000000000000)

//================

//[DOR] Some additional two-operand instructions require a register source operand
//                       0b0111xxx000000000
#define OP_MUL  ((int16_t) 0070000) // Multiply
#define OP_DIV  ((int16_t) 0071000) // Divide
#define OP_ASH  ((int16_t) 0072000)
#define OP_ASHC ((int16_t) 0073000)
#define OP_XOR  ((int16_t) 0074000) // Exclusive OR
#define OP_SOB  ((int16_t) 0077000) // Subtract one and branch (if not = 0)
//                        0b0111xxx000000000
//#define OP_xxx (int16_t) 0b0111101000000000  (floating point operations)
//#define OP_yyy (int16_t) 0b0111110000000000  (system instruction)
#define MASK_DOIR        ((int16_t) 0b1111000000000000)
#define VALUE_DOIR       ((int16_t) 0b0111000000000000)
#define MASK_OPCODE_DOIR ((int16_t) 0b0111111000000000)

//================

//[SO] single-operand instruction opcodes
//                       0b00001xxxxx000000
#define OP_CLR  ((int16_t) 0005000) // Clear
#define OP_CLRB ((int16_t) 0105000)
#define OP_COM  ((int16_t) 0005100) // Complement
#define OP_COMB ((int16_t) 0105100)
#define OP_INC  ((int16_t) 0005200) // Increment
#define OP_INCB ((int16_t) 0105200)
#define OP_DEC  ((int16_t) 0005300) // Decrement
#define OP_DECB ((int16_t) 0105300)
#define OP_NEG  ((int16_t) 0005400) // Negate
#define OP_NEGB ((int16_t) 0105400)
#define OP_TST  ((int16_t) 0005700) // Test
#define OP_TSTB ((int16_t) 0105700)
#define OP_ASR  ((int16_t) 0006200) // Arithmetic Shift Right
#define OP_ASRB ((int16_t) 0106200)
#define OP_ASL  ((int16_t) 0006300) // Arithmetic Shift Left
#define OP_ASLB ((int16_t) 0106300)
#define OP_ROR  ((int16_t) 0006000) // Rotate Right
#define OP_RORB ((int16_t) 0106000)
#define OP_ROL  ((int16_t) 0006100) // Rotate Left
#define OP_ROLB ((int16_t) 0106100)
#define OP_SWAB ((int16_t) 0000300) // Swap Bytes
#define OP_ADC  ((int16_t) 0005500) // Add Caryy
#define OP_ADCB ((int16_t) 0105500)
#define OP_SBC  ((int16_t) 0005600) // Substract Carry
#define OP_SBCB ((int16_t) 0105600)
#define OP_SXT  ((int16_t) 0006700) // Sign Extend
//                        0b00001xxxxx000000
//                        0b0^^^^00000000000
#define MASK_SOI        ((int16_t) 0b0111100000000000)
#define VALUE_SOI       ((int16_t) 0b0000100000000000)
#define MASK_OPCODE_SOI ((int16_t) 0177700)

//================

//[B] condition branch instruction opcodes
//                       0b00000xxxxx000000
#define OP_BR   ((int16_t) 0000400) // Branch unconditional
#define OP_BNE  ((int16_t) 0001000) // Branch if not equal
#define OP_BEQ  ((int16_t) 0001400) // Branch if Equal (to zero)
#define OP_BPL  ((int16_t) 0100000) // Branch if plus
#define OP_BMI  ((int16_t) 0100400) // Branch if minus
#define OP_BVC  ((int16_t) 0102000) // Branch if V bit clear
#define OP_BVS  ((int16_t) 0102400) // Branch if V bit set
#define OP_BCC  ((int16_t) 0103000) // Branch if Carry Clear
#define OP_BCS  ((int16_t) 0103400) // Branch if Carry Set
#define OP_BGE  ((int16_t) 0002000) // Branch if greater than or equal
#define OP_BLT  ((int16_t) 0002400) // Branch if less than
#define OP_BGT  ((int16_t) 0003000) // Branch if greater than
#define OP_BLE  ((int16_t) 0003400) // Branch if less than or equal to
#define OP_BHI  ((int16_t) 0101000) // Branch if higher
#define OP_BLOS ((int16_t) 0101400) // Branch if lower or same
//#define OP_BHIS ((int16_t) 0103000) // Branch if higher than or same
//#define OP_BLO  ((int16_t) 0103400) // Branch if lower
//                        0b00000xxxxx000000
//                        0b0^^^^00000000000
#define MASK_B        ((int16_t) 0b0111100000000000)
#define MASK_OPCODE_B ((int16_t) 0b0000011100000000)
#define MASK_OFFSET_B ((int16_t) 0b0000000011111111)

//================

// Traps and Interrupts
#define OP_EMT  ((int16_t) 0104000) // ??? Emulator Trap
#define OP_TRAP ((int16_t) 0104400) // ??? Trap
#define OP_BPT  ((int16_t) 0000003) // Breakpoint Trap
#define OP_IOT  ((int16_t) 0000004) // I/O Trap
#define OP_CSM  ((int16_t) 0007000) // Call to Supervisor Mode
#define OP_RTI  ((int16_t) 0000002) // Return From Interrupt
#define OP_RTT  ((int16_t) 0000006) // Return From Interrupt

//================

//[MS] miscellaneous instructions / without operand instructions opcode
#define OP_HALT  ((int16_t) 0000000) //
#define OP_WAIT  ((int16_t) 0000001) // Wait for Interrupt
#define OP_RESET ((int16_t) 0000005) // Reset
#define OP_MTPD  ((int16_t) 0106600) // Move to previous Data Space
#define OP_MTPI  ((int16_t) 0006600) // Move to previous Instruction Space
#define OP_MFPD  ((int16_t) 0106500) // Move From Previous Data Space
#define OP_MFPI  ((int16_t) 0006500) // Move From Previous Instruction Space
#define OP_MTPS  ((int16_t) 0106400) // Move Byte to PSW
#define OP_MFPS  ((int16_t) 0106700) // Move Byte From PSW
#define OP_MFPT  ((int16_t) 0000007) // Move From Processor
#define OP_XFC   ((int16_t) 0076700) // [SO] Extended Function Code
//                        0b00000000000000xx

//================

// [CC] Condition Code Operation
#define OP_CLC ((int16_t) 0000257) // Clear C
#define OP_CLV ((int16_t) 0000242) // Clear V
#define OP_CLZ ((int16_t) 0000244) // Clear Z
#define OP_CLN ((int16_t) 0000250) // Clear N
#define OP_CCC ((int16_t) 0000241) // Clear All Condition Bits
#define OP_SEC ((int16_t) 0000261) // Set All
#define OP_SEV ((int16_t) 0000262) // Set C
#define OP_SEZ ((int16_t) 0000264) // Set Z
#define OP_SEN ((int16_t) 0000270) // Set N
#define OP_SCC ((int16_t) 0000277) // Set All

//================
// Jump and Subroutine Instruction
#define OP_JMP  ((int16_t) 0000100) // Jump
#define OP_JSR  ((int16_t) 0004000) // Jump to Subroutine
#define OP_RTS  ((int16_t) 0000200) // ??? Return from subroutine
#define OP_MARK ((int16_t) 0006400) //

//================

// [GRAM] General Register Addressing Modes
#define GRAM_REG     ((int16_t) 0) // register: the operand is in Rn
#define GRAM_REG_DEF ((int16_t) 1) // register deferred: Rn contains the
                                   // address of the operand
#define GRAM_INC     ((int16_t) 2) // autoincrement: Rn contains the address
                                   // of the operand, then increment Rn
#define GRAM_INC_DEF ((int16_t) 3) // autoincrement deferred: Rn contains
                                   // the address of the address, then
                                   // increment Rn by 2
#define GRAM_DEC     ((int16_t) 4) // autodecremet: Decrement Rn, then use
                                   // it as the address
#define GRAM_DEC_DEF ((int16_t) 5) // autodecremet deferred: Decrement Rn
                                   // by 2, then use it as the address of
                                   // the address
#define GRAM_IND     ((int16_t) 6) // index: Rn+X is the address of  the
                                   // operand
#define GRAM_IND_DEF ((int16_t) 7) // index deferred: Rn+X is the address
                                   // of the address

//================

// [PCAM] Program Counter Addressing Modes
#define PCAM_IMM     ((int16_t) 2) // Immediate: The operand is contained
                                   // in the instruction
#define PCAM_ABS     ((int16_t) 3) // Absolute: The absolute address is
                                   // contained in the instruction
#define PCAM_REL     ((int16_t) 6) // Relative: An extra word in the
                                   // instruction is added to PC+2 to
                                   // give the address
#define PCAM_REL_DEF ((int16_t) 7) // Relative deferred: An extra word in
                                   // the instruction is added to PC+2 to
                                   // give the address of the address

//================

// Stack Addressing Modes
#define SAM_DEF     ((int16_t) 1) // Deferred: The operand is on the
                                  // top of the stack
#define SAM_INC     ((int16_t) 2) // Autoincrement: The operand is on
                                  // the top of stack, then pop it off
#define SAM_INC_DEF ((int16_t) 3) // Autoincrement deferred: A pointer
                                  // to the operand is on top of the
                                  // stack; pop the pointer off
#define SAM_DEC     ((int16_t) 4) // Autodecrement: Push a value onto
                                  // the stack
#define SAM_IND     ((int16_t) 6) // Indexed: This refers to any item
                                  // on the stack by its positive
                                  // distance from the top
#define SAM_IND_DEF ((int16_t) 7) // Indexed deferred: This refers to
                                  // a value to which a pointer is at
                                  // the specified location on the stack

//================

#define MASK_FIRST_ADDR_MODE  ((int16_t) 0007000)
#define MASK_SECOND_ADDR_MODE ((int16_t) 0000070)

//================

// code registers
#define R0 ((int16_t) 0)
#define R1 ((int16_t) 1)
#define R2 ((int16_t) 2)
#define R3 ((int16_t) 3)
#define R4 ((int16_t) 4)
#define R5 ((int16_t) 5)
#define R6 ((int16_t) 6)
#define R7 ((int16_t) 7)
#define MASK_FIRST_REG  ((int16_t) 0000700)
#define MASK_SECOND_REG ((int16_t) 0000007)

//================
#define MASK_FIRST_OPERAND (MASK_FIRST_ADDR_MODE | MASK_FIRST_REG)
#define MASK_SECOND_OPERAND (MASK_SECOND_ADDR_MODE | MASK_SECOND_REG)
//================

enum InstructionType {
    // Double operand instructions
    Mov, Movb, Cmp, Cmpb, Bit, Bitb, Bic, Bicb, Bis, Bisb, Add, Sub,
    // Double operand instructions with register
    Mul, Div, Ash, Ashc, Exor,

    // Single operand instructions
    Clr, Clrb, Com, Comb, Inc, Incb, Dec, Decb, Neg, Negb, Tst, Tstb,
    Asr, Asrb, Asl, Aslb, Ror, Rorb, Rol, Rolb, Swab,
    Adc, Adcb, Sbc, Sbcb, Sxt,

    // Branch instructions
    Br, Bne, Beq, Bpl, Bmi, Bvc, Bvs, Bcc, Bcs,
    Bge, Blt, Bgt, Ble, Sob, Bhi, Blos, Bhis, Blo,

    // Jump and Subroutine
    Jmp, Jsr, Rts, Mark,

    // No operand instruction
    // Traps and Interrupts
    Emt, Trap, Bpt, Iot, Csm, Rti, Rtt,

    //Condition Code Operation
    Clc, Clv, Clz, Cln, Ccc, Sec, Sev, Sez, Sen, Scc,

    // Miscellaneous Instructions
    Halt, Wait, Reset, Mtpd, Mtpi, Mfpd, Mfpi, Mtps, Mfps, Mfpt
    };

const static std::string instructionTypesMap[] = {
    "MOV", "MOVB", "CMP", "CMPB", "BIT", "BITB", "BIC", "BICB", "BIS",
    "BISB", "ADD", "SUB",
    "MUL", "DIV", "ASH", "ASHC", "XOR",
    "CLR", "CLRB", "COM", "COMB", "INC", "INCB", "DEC", "DECB", "NEG", "NEGB", "TST", "TSTB",
    "ASR", "ASRB", "ASL", "ASLB", "ROR", "RORB", "ROL", "ROLB", "SWAB",
    "ADC", "ADCB", "SBC", "SBCB", "SXT",
    "BR", "BNE", "BEQ", "BPL", "BMI", "BVC", "BVS", "BCC", "BCS",
    "BGE", "BLT", "BGT", "BLE", "SOB", "BHI", "BLOS", "BHIS", "BLO",
    "JMP", "JSR", "RTS", "MARK",
    "EMT", "TRAP", "BPT", "IOT", "CSM", "RTI", "RTT",
    "CLC", "CLV", "CLZ", "CLN", "CCC", "SEC", "SEV", "SEZ", "SEN", "SCC",
    "HALT", "WAIT", "RESET", "MTPD", "MTPI", "MFPD", "MFPI", "MTPS", "MFPS", "MFPT"
};

enum AddressingMode {
    // [GRAM] General Register Addressing Modes
    GR_Register, GR_RegisterDef, GR_Increment, GR_IncrementDef,
    GR_Decrement, GR_DecrementDef, GR_Index, GR_IndexDef,
    // [PCAM] Program Counter Addressing Modes
    PC_Immediate, PC_Absolute, PC_Relative, PC_RelativeDef,
    // [SAM] Stack Addressing Modes
    ST_Deferred, ST_Increment, ST_IncrementDef, ST_Decrement,
    ST_Index, ST_IndexDef
};

const static std::string addressingModesMap[] = {
    "GR_REGISTER", "GR_REGISTERDEF", "GR_INCREMENT", "GR_INCREMENTDEF",
    "GR_DECREMENT", "GR_DECREMENTDEF", "GR_INDEX", "GR_INDEXDEF",
    "PC_IMMEDIATE", "PC_ABSOLUTE", "PC_RELATIVE", "PC_RELATIVEDEF",
    "ST_DEFERRED", "ST_INCREMENT", "ST_INCREMENTDEF", "ST_DECREMENT",
    "ST_INDEX", "ST_INDEXDEF"
};


class InstructionOperand
{
public:
    InstructionOperand();
    InstructionOperand(const InstructionOperand& io);
    InstructionOperand& operator =(const InstructionOperand& io);
    int16_t reg() const;
    AddressingMode mode() const;
    void setReg(const int16_t& r);
    void setMode(const AddressingMode& m);
private:
    int16_t m_reg;
    AddressingMode m_mode;
};

class Instruction
{
public:
    Instruction();
    Instruction(int16_t addr, InstructionType type,
                InstructionOperand *firstOp, InstructionOperand *secondOp,
                int16_t offset, int complexity);
    Instruction(const Instruction& i);
    Instruction(Instruction&& i);
    Instruction& operator=(const Instruction& i);
    Instruction& operator=(Instruction&& i);
    ~Instruction();
    int16_t address() const;
    InstructionType type() const;
    InstructionOperand *first();
    InstructionOperand *second();
    int16_t offset() const;
    int16_t complexity() const;

    static AddressingMode decodeAddrMode(int16_t opcode);
    static AddressingMode decodeStackAddrMode(int16_t opcode);
    static AddressingMode decodeProgCountAddrMode(int16_t opcode);
    static AddressingMode decodeGenRegAddrMode(int16_t opcode);
    static Instruction decodeDoubleOperandInstruction(int16_t mw);
    static Instruction decodeDoubleOperandInstructionRegister(int16_t mw);
    static Instruction decodeSingleOperandInstruction(int16_t mw);
    static Instruction decodeBranchInstruction(int16_t mw);
    static Instruction decodeNoOperandInstruction(int16_t mw);
    static Instruction decodeConditionCode(int16_t mw);
    static Instruction decode(int16_t mw);

private:
    int16_t m_addr;
    InstructionType m_type;
    InstructionOperand *m_first;
    InstructionOperand *m_second;
    int16_t m_offset;
    int m_complexity;
};

std::ostream& operator<<(std::ostream& os, Instruction& i);

#endif // INSTRUCTION_H
