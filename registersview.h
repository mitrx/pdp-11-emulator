#ifndef REGISTERSVIEW_H
#define REGISTERSVIEW_H

#include <QObject>
#include "emulator.h"

class RegistersView : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString text READ text NOTIFY textChanged)
public:
    explicit RegistersView(Emulator& emulator, QObject *parent = 0);
    ~RegistersView();
    Q_INVOKABLE void render();
    QString text();
signals:
    void textChanged();
private:
    Registers& m_registers;
    QString *m_text;
};

#endif // REGISTERSVIEW_H
