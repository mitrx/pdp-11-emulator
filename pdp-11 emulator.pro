TEMPLATE = app

QT += qml quick gui widgets
CONFIG += c++11

SOURCES += main.cpp \
    disassemblerview.cpp \
    instruction.cpp \
    hexviewerview.cpp \
    registersview.cpp \
    monitorview.cpp \
    registers.cpp \
    memorychunk.cpp \
    monitorcontroller.cpp \
    pipeline.cpp \
    memory.cpp \
    cpu.cpp \
    emulator.cpp \
    pipelinestate.cpp \
    encoder.cpp \
    alu.cpp \
    testencoding.cpp \
    emulatorcontroller.cpp

RESOURCES += \
    qml.qrc

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    disassemblerview.h \
    instruction.h \
    hexviewerview.h \
    registersview.h \
    monitorview.h \
    pipelinestate.h \
    registers.h \
    memorychunk.h \
    monitorcontroller.h \
    pipeline.h \
    memory.h \
    cpu.h \
    emulator.h \
    config.h \
    encoder.h \
    alu.h \
    testencoding.h \
    emulatorcontroller.h
