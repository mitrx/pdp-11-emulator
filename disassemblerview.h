#ifndef DISASSEMBLERVIEW_H
#define DISASSEMBLERVIEW_H
#include <QObject>
#include <QString>
#include "emulator.h"

class DisassemblerView: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString text READ text NOTIFY textChanged)
public:
    explicit DisassemblerView(Emulator& emulator, QObject *parent = 0);
    ~DisassemblerView();
    Q_INVOKABLE void render();
    QString text() const;
signals:
    void textChanged();
private:
    Memory& m_memory;
    QString *m_text;
};

#endif // DISASSEMBLERVIEW_H
