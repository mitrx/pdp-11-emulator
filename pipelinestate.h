#ifndef PIPELINESTATE_H
#define PIPELINESTATE_H

#include "config.h"
#include "instruction.h"

struct PipelineState
{
public:
    PipelineState();

    // fetch variables
    uint16_t fetchedInstruction;
    bool fetchFinished;
    int fetchCycles;
    // decode variables
    uint16_t decodingInstuction;
    Instruction decodedInstruction;
    int decodeCycles;
    bool decodeFinished;
    // operand fetch variables
    Instruction operandFetchingInstruction;
    uint16_t operandFetchFirst;
    uint16_t operandFetchSecond;
    bool operandFetchFinished;
    int operandFetchCycles;
    // instructions execution variables
    Instruction executingInstruction;
    uint16_t executingFirst;
    uint16_t executingSecond;
    uint16_t executionResult;
    bool executionFinished;
    int executionCycles;
    // write back variables
    Instruction executedInstruction;
    uint16_t writeBackResult;
    bool writeBackFinished;
    int writeBackCycles;
    // general
    int completedCount;
    bool halt;
};

#endif // PIPELINESTATE_H
