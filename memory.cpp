#include "memory.h"

Memory::Memory()
{
    data = new uint16_t[MEMORY_AMOUNT / sizeof(uint16_t)];
    ivtLimit = 0;
#ifdef __MEMORY_DEBUG__
    std::cout << "MEMORY_AMOUNT = "<< std::oct << MEMORY_AMOUNT << std::endl;
    std::cout << "RAM_START = " << std::oct << RAM_START << std::endl;
    std::cout << "RAM_LIMIT = " << std::oct << RAM_LIMIT << std::endl;
    std::cout << "VRAM_START = " << std::oct << VRAM_START << std::endl;
    std::cout << "VRAM_LIMIT = " << std::oct << VRAM_LIMIT << std::endl;
    std::cout << "ROM_START = " << std::oct << ROM_START << std::endl;
    std::cout << "ROM_LIMIT = " << std::oct << ROM_LIMIT << std::endl;
    std::cout << "IO_START = " << std::oct << IO_START << std::endl;
    std::cout << "IO_LIMIT = " << std::oct << IO_LIMIT << std::endl;
#endif
}

void Memory::clear()
{
    memset(data, 0, MEMORY_AMOUNT);
}

void Memory::initialize()
{
    FILE *f = nullptr;
    f = fopen(MEM_INIT_FILE, "b");
    if (!f) {
        std::cout << "file open error" << std::endl;
    } else {
        uint16_t addr = getRomStart();
        while (!feof(f) && addr < ROM_LIMIT) {
            int16_t word;
            fscanf(f, "%hu", &word);
            writeRom(addr++, word);
        }
        fclose(f);
    }
}

uint16_t Memory::ivtAddr(uint16_t addr)
{
    // it should be checked that the offset is not negative
    // and offset is in limits of IVT
    if (addr >= 0 && addr < ivtLimit) {
        return data[addr / MW_LENGTH];
    } else {
#ifdef __MEMORY_DEBUG__
        std::cout << "offset is either negative or exceeds ivt limits" << std::endl;
#endif
        throw invalidMemAddrEx;
    }
}

uint16_t Memory::ramAddr(uint16_t addr)
{
    // it should be checked that offset is not negative
    // and doesn't exceed RAM limits
    if (addr >= 0 && RAM_START + addr < RAM_LIMIT) {
        return data[addr / MW_LENGTH];
    } else {
#ifdef __MEMORY_DEBUG__
        std::cout << "offset is either negative or exceeds RAM limits" << std::endl;
#endif
        throw invalidMemAddrEx;
    }
}

uint16_t Memory::vramAddr(uint16_t addr)
{
    // it should be checked that offset is not negative
    // and doesn't exceed VRAM limits
    if (addr >= 0 && VRAM_START + addr < VRAM_LIMIT) {
        return data[addr / MW_LENGTH];
    } else {
#ifdef __MEMORY_DEBUG__
        std::cout << "offset is either negative or exceeds VRAM limits" << std::endl;
#endif
        throw invalidMemAddrEx;
    }
}

uint16_t Memory::romAddr(uint16_t addr)
{
    // it should be checked that offset is not negative
    // and doesn't exceed ROM limits
    if (addr >= 0 && ROM_START + addr < ROM_LIMIT) {
        return data[addr / MW_LENGTH];
    } else {
#ifdef __MEMORY_DEBUG__
        std::cout << "offset is either negative or exceeds ROM limits" << std::endl;
#endif
        throw invalidMemAddrEx;
    }
}

uint16_t Memory::ioAddr(uint16_t addr)
{
    if (addr >= 0 && IO_START + addr < IO_LIMIT) {
        return data[addr / MW_LENGTH];
    } else {
#ifdef __MEMORY_DEBUG__
        std::cout << "offset is either negative or exceeds IO limits" << std::endl;
#endif
        throw invalidMemAddrEx;
    }
}

uint16_t Memory::getWord(uint16_t addr)
{
    return data[addr / MW_LENGTH];
}

void Memory::setIvtLimit(uint16_t limit)
{
    if (limit >= 0 && limit < RAM_LIMIT) {
        ivtLimit = limit;
    } else {
#ifdef __MEMORY_DEBUG__
        std::cout << "invalid IVT limit value" << std::endl;
#endif
    }
}

bool Memory::writeRam(uint16_t addr, uint16_t value)
{
    if (addr >= RAM_START && addr < RAM_LIMIT) {
        data[addr / MW_LENGTH] = value;
#ifdef __MEMORY_DEBUG__
        std::cout << "RAM value successfully written" << std::endl;
#endif
        return true;
    } else {
#ifdef __MEMORY_DEBUG__
        std::cout << "RAM value can't be written to this address" << std::endl;
#endif
        throw invalidMemAddrEx;
    }
}

bool Memory::writeVram(uint16_t addr, uint16_t value)
{
    if (addr >= VRAM_START && addr < VRAM_LIMIT) {
        data[addr / MW_LENGTH] = value;
#ifdef __MEMORY_DEBUG__
        std::cout << "VRAM value successfully written" << std::endl;
#endif
        return true;
    } else {
#ifdef __MEMORY_DEBUG__
        std::cout << "VRAM value can't be written to this address" << std::endl;
#endif
        throw invalidMemAddrEx;
    }
}

void Memory::writeWord(uint16_t addr, uint16_t value)
{
    data[addr / MW_LENGTH] = value;
}

bool Memory::writeRom(uint16_t addr, uint16_t value)
{
    if (addr >= ROM_START && addr < ROM_LIMIT) {
        data[addr / MW_LENGTH] = value;
#ifdef __MEMORY_DEBUG__
        std::cout << "ROM value successfully written" << std::endl;
#endif
        return true;
    } else {
#ifdef __MEMORY_DEBUG__
        std::cout << "ROM value can't be written to this address" << std::endl;
#endif
        throw invalidMemAddrEx;
    }
}

uint16_t Memory::getRomStart()
{
    return ROM_START;
}

Memory::~Memory()
{
    delete[] data;
}
