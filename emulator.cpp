#include "emulator.h"
#include <thread>

Emulator::Emulator()
{
    m_memory = new Memory();
    m_cpu = new Cpu(*m_memory);
}

void Emulator::initialize()
{
    std::cout << "before init" << std::endl;
    m_cpu->initialize();
    std::cout << "after init, before mem init" << std::endl;
    m_memory->initialize();
    std::cout << "after mem init" << std::endl;
    m_memory->writeRom(m_memory->getRomStart(), 0005200);
    m_memory->writeRom(m_memory->getRomStart() + 2, 0000000);
    m_initialized = true;
    m_finished = false;
    m_stop = false;
    m_wait = false;
}

void Emulator::run()
{
    m_stop = false;
    m_wait = false;
#ifdef __EMULATOR_DEBUG__
    std::cout << "Inside Emulator::run" << std::endl;
#endif
    if (m_initialized && !m_finished) {
        std::thread cpuThread = std::thread(&Emulator::runLoop, this);
//        cpuThread.detach();
//        if (TRACK_EXECUTION) {
//            std::thread trackigThread = std::thread(&Emulator::trackLoop, this);
//            trackigThread.detach();
//        }
        cpuThread.join();
    }
}

void Emulator::step()
{
    if (m_initialized && !m_finished) {
        m_cpu->cycle();
    }
}

void Emulator::stop()
{
    m_stop = true;
}

void Emulator::reset()
{
     m_cpu->reset();
     m_memory->clear();
     m_memory->initialize();
}

void Emulator::runLoop(Emulator *self)
{
    while (!self->m_finished && !self->m_stop) {
        self->m_finished = self->m_cpu->cycle();
#ifdef __EMULATOR_DEBUG__
        std::cout << "execution finished? " << std::endl;
        std::cout << "finished? " << self->m_finished << std::endl;
        std::cout << "stopped? " << self->m_stop << std::endl;
        std::cout << "result? " << (!self->m_finished && !self->m_stop) << std::endl;
#endif
        if (self->m_wait) {
            std::this_thread::sleep_for(self->m_sleepAmount);
            self->m_wait = false;
        }
    }
    self->m_stop = true;
}

void Emulator::trackLoop(Emulator *self)
{
    static SlowCpuException slowCpuException;
    int cycleDuration = std::chrono::nanoseconds::period::den / self->m_cpu->frequency();
    std::chrono::nanoseconds validationInterval(CLOCK_VALIDATION_INTERVAL);
#ifdef __EMULATOR_DEBUG__
    std::cout << "cpuCycleDuration = " << cycleDuration << std::endl;
#endif
    auto startTime = std::chrono::high_resolution_clock::now();
    while (self->m_stop == false) {
        std::this_thread::sleep_for(validationInterval);
        // calculate the duration the cpu runs in nanoseconds
        auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(
                    std::chrono::high_resolution_clock::now() - startTime).count();
        std::chrono::nanoseconds timeLeft(cycleDuration * self->m_cpu->cycles() - duration);
        if (timeLeft.count() < 0) {
            // notify that emulator lacks performance
#ifdef __EMULATOR_DEBUG__
            std::cout << "slower by " << timeLeft.count() << ", cycles " <<
                         self->cpu().cycles() << std::endl;
#endif
            throw slowCpuException;
        } else {
            // sleep the left time
#ifdef __EMULATOR_DEBUG__
            std::cout << "waiting for " << timeLeft.count() << std::endl;
#endif
            self->m_sleepAmount = timeLeft;
            self->m_wait = true;
        }
    }
}

Cpu& Emulator::cpu()
{
    return *m_cpu;
}

Memory& Emulator::memory()
{
    return *m_memory;
}

Emulator::~Emulator()
{
    delete m_cpu;
    delete m_memory;
}
