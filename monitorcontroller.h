#ifndef MONITORCONTROLLER_H
#define MONITORCONTROLLER_H

#include <QObject>
#include <QDebug>
#include "monitorview.h"

class MonitorController: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString source READ source NOTIFY sourceChanged)
public:
    MonitorController(MonitorView& view, QObject *parent = nullptr);
    Q_INVOKABLE void fillPictureWithRed();
    Q_INVOKABLE void render();
    QString source();
signals:
    void sourceChanged();
private:
    MonitorView&  m_view;
    QString m_source;
    int pictureCount;
};

#endif // MONITORCONTROLLER_H
