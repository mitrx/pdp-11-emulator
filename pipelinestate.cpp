#include "pipelinestate.h"

PipelineState::PipelineState()
{
    fetchFinished = false;
    decodeFinished = false;
    executionFinished = false;
    operandFetchFinished = false;
    writeBackFinished = false;
    fetchedInstruction = -1;
    writeBackResult = -1;
    fetchCycles = MEMORY_ACCESS_DURATION / CYCLE_DURATION;
    operandFetchCycles = -1;
    executionCycles = -1;
    writeBackCycles = -1;
    decodeCycles = -1;
    completedCount = 0;
    halt = false;
}
