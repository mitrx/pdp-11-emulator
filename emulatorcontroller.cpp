#include "emulatorcontroller.h"

EmulatorController::EmulatorController(Emulator& emulator, QObject *parent)
    : QObject(parent), m_emulator(emulator)
{
}

void EmulatorController::initialize()
{
    m_emulator.initialize();
}

void EmulatorController::run()
{
    m_emulator.run();
}

void EmulatorController::step()
{
    m_emulator.step();
}

void EmulatorController::reset()
{
    m_emulator.reset();
}
