#include "registers.h"

Registers::Registers()
{
    m_registers = new uint16_t[9];
}

void Registers::reset()
{
    for (int i = 0; i < 9; i++) {
        m_registers[i] = 0;
    }
}

void Registers::setR0(uint16_t val)
{
    m_registers[0] = val;
}

void Registers::setR1(uint16_t val)
{
    m_registers[1] = val;}

void Registers::setR2(uint16_t val)
{
    m_registers[2] = val;}

void Registers::setR3(uint16_t val)
{
    m_registers[3] = val;}

void Registers::setR4(uint16_t val)
{
    m_registers[4] = val;}

void Registers::setR5(uint16_t val)
{
    m_registers[5] = val;
}

void Registers::setSp(uint16_t val)
{
    m_registers[6] = val;
}

void Registers::setPc(uint16_t val)
{
    m_registers[7] = val;
}

void Registers::setPsw(uint16_t val)
{
    m_registers[8] = val;
}

void Registers::setReg(uint16_t number, uint16_t val)
{
    m_registers[number] = val;
}

int16_t Registers::getReg(uint16_t number)
{
    if ((number >= 0) && (number <= 8))
        return m_registers[number];
    else
        return 0;
}

uint16_t Registers::r0()
{
    return m_registers[0];
}

uint16_t Registers::r1()
{
    return m_registers[1];
}

uint16_t Registers::r2()
{
    return m_registers[2];
}

uint16_t Registers::r3()
{
    return m_registers[3];
}

uint16_t Registers::r4()
{
    return m_registers[4];
}

uint16_t Registers::r5()
{
    return m_registers[5];
}

uint16_t Registers::sp()
{
    return m_registers[6];
}

uint16_t Registers::pc()
{
    return m_registers[7];
}

uint16_t Registers::psw()
{
    return m_registers[8];
}

void Registers::setN()
{
    m_registers[8] = m_registers[8] | MASK(3);
}

void Registers::setZ()
{
    m_registers[8] = m_registers[8] | MASK(2);
}

void Registers::setV()
{
    m_registers[8] = m_registers[8] | MASK(1);
}

void Registers::setC()
{
    m_registers[8] = m_registers[8] | MASK(0);
}

void Registers::clearN()
{
    m_registers[8] = m_registers[8] & (~MASK(3));
}

void Registers::clearZ()
{
    m_registers[8] = m_registers[8] & (~MASK(2));
}

void Registers::clearV()
{
    m_registers[8] = m_registers[8] & (~MASK(1));
}

void Registers::clearC()
{
    m_registers[8] = m_registers[8] & (~MASK(0));
}

bool Registers::getN()
{
    return (m_registers[8] & MASK(3)) != 0;
}

bool Registers::getZ()
{
    return (m_registers[8] & MASK(2)) != 0;
}

bool Registers::getV()
{
    return (m_registers[8] & MASK(1)) != 0;
}

bool Registers::getC()
{
    return (m_registers[8] & MASK(0)) != 0;
}

//int16_t Registers::

uint16_t Registers::registerAt(int pos)
{
    if (pos >= 0 && pos < 9) {
        return m_registers[pos];
    } else {
        throw invalidRegEx;
    }
}

void Registers::setRegisterAt(int pos, uint16_t value)
{
    if (pos >= 0 && pos < 9) {
        m_registers[pos] = value;
    } else {
        throw invalidRegEx;
    }
}

Registers::~Registers()
{
    delete[] m_registers;
}
