#ifndef EMULATORCONFIG_H
#define EMULATORCONFIG_H

#include <cstdint>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define __PDP_11_DEBUG__

#ifdef __PDP_11_DEBUG__
#include <iostream>
#define __EMULATOR_DEBUG__
#define __CPU_DEBUG__
#define __PIPELINE_DEBUG__
#define __PIPELINE_STATE_DEBUG__
#define __INSTRUCTION_DEBUG__
#define __REGISTERS_DEBUG__
#define __MEMORY_DEBUG__
#define __ENCODER_DEBUG__
#define __ALU_DEBUG__
#define __TESTENCODING_DEBUG__
#endif

#define MW_LENGTH 2 // 2 bytes machine word length

#define TRACK_EXECUTION 0   // do we launch tracking thread?
#define NSECONDS_IN_SECOND 1000000000                       // number of nanoseconds in a second
#define CPU_FREQUENCY 5000000                               // 5 MHz
#define CLOCK_VALIDATION_INTERVAL 1000000                   // time should pass until clock reconciliation, ns
#define MEMORY_FETCH_INTERVAL 1000                          // time it takes to fetch a MW from memory, ns
#define MEMORY_ACCESS_DURATION 1000                         // time it takes to bring a machine word from memory, ns
#define CYCLE_DURATION (NSECONDS_IN_SECOND / CPU_FREQUENCY) // tact duration, ns

#define MEM_INIT_FILE "meminit.bin"

#endif // EMULATORCONFIG_H
