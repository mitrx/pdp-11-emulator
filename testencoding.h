#ifndef TESTENCODING_H
#define TESTENCODING_H

#include "config.h"
#include "instruction.h"
#include "encoder.h"

const int N = 10; // number of instruction

Instruction *createInstrList();
void testEncoding();
#endif // TESTENCODING_H
