#include "encoder.h"

// [GRAM] General Register Addressing Modes
int16_t encodeGenRegAddrMode(const AddressingMode mode)
{
    switch (mode) {
        case AddressingMode::GR_Register:
            return GRAM_REG;
        case AddressingMode::GR_RegisterDef:
            return GRAM_REG_DEF;
        case AddressingMode::GR_Increment:
            return GRAM_INC;
        case AddressingMode::GR_IncrementDef:
            return GRAM_INC_DEF;
        case AddressingMode::GR_Decrement:
            return GRAM_DEC;
        case AddressingMode::GR_DecrementDef:
            return GRAM_DEC_DEF;
        case AddressingMode::GR_Index:
            return GRAM_IND;
        case AddressingMode::GR_IndexDef:
            return GRAM_IND_DEF;
        default:
#ifdef __ENCODER_DEBUG__
            std::cout << "Addresing Mode doesn't exist!" << std::endl;
#endif
        return GRAM_REG;
    }
}

// [PCAM] Program Counter Addressing Modes
int16_t encodeProgCountAddrMode(const AddressingMode mode)
{
    switch (mode) {
    case AddressingMode::PC_Immediate:
        return PCAM_IMM;
    case AddressingMode::PC_Absolute:
        return PCAM_ABS;
    case AddressingMode::PC_Relative:
        return PCAM_REL;
    case AddressingMode::PC_RelativeDef:
        return PCAM_REL_DEF;
    default:
#ifdef __ENCODER_DEBUG__
        std::cout << "Addresing Mode doesn't exist!" << std::endl;
#endif
        return PCAM_IMM;
    }
}

// [SAM] Stack Addressing Modes
int16_t encodeStackAddrMode(const AddressingMode mode)
{
    switch (mode) {
    case AddressingMode::ST_Deferred:
        return SAM_DEF;
    case AddressingMode::ST_Increment:
        return SAM_INC;
    case AddressingMode::ST_IncrementDef:
        return SAM_INC_DEF;
    case AddressingMode::ST_Decrement:
        return SAM_DEC;
    case AddressingMode::ST_Index:
        return SAM_IND;
    case AddressingMode::ST_IndexDef:
        return SAM_IND_DEF;
    default:
#ifdef __ENCODER_DEBUG__
        std::cout << "Addresing Mode doesn't exist!" << std::endl;
#endif
        return SAM_DEF;
    }
}

int16_t encodeAddrMode(const InstructionOperand operand)
{
    if (operand.reg() == R6)
        return encodeStackAddrMode(operand.mode());
    else if (operand.reg() == 7)
        return encodeProgCountAddrMode(operand.mode());
    else
        return encodeGenRegAddrMode(operand.mode());
}

int16_t encodeDoubleOperandInstr(Instruction instr)
{
    int16_t code;
    code = (instr.first()->reg() << 6) | (instr.second()->reg());
    code = code | (encodeAddrMode(*instr.first()) << 9) |
            (encodeAddrMode(*instr.second()) << 3);
    return code;
}

int16_t encodeDoubleOperandRegInstr(Instruction instr)
{
    int16_t code;
    code = (instr.first()->reg() << 6) | (instr.second()->reg());
    code = code | (encodeAddrMode(*instr.second()) << 3);
    return code;
}

int16_t encodeSingleOperandInstr(Instruction instr)
{
    return (instr.first()->reg()) | (encodeAddrMode(*instr.first()) << 3);
}

int16_t encodeOneInstruction(const Instruction instr)
{

    switch (instr.type()) {
//    [DO] douoble operand instructions
    case InstructionType::Mov:
        return OP_MOV | encodeDoubleOperandInstr(instr);
    case InstructionType::Movb:
        return OP_MOVB | encodeDoubleOperandInstr(instr);
    case InstructionType::Cmp:
        return OP_CMP | encodeDoubleOperandInstr(instr);
    case InstructionType::Cmpb:
        return OP_CMPB | encodeDoubleOperandInstr(instr);
    case InstructionType::Bit:
        return OP_BIT | encodeDoubleOperandInstr(instr);
    case InstructionType::Bitb:
        return OP_BITB | encodeDoubleOperandInstr(instr);
    case InstructionType::Bic:
        return OP_BIC | encodeDoubleOperandInstr(instr);
    case InstructionType::Bicb:
        return OP_BICB | encodeDoubleOperandInstr(instr);
    case InstructionType::Bis:
        return OP_BIS | encodeDoubleOperandInstr(instr);
    case InstructionType::Bisb:
        return OP_BISB | encodeDoubleOperandInstr(instr);
    case InstructionType::Add:
        return OP_ADD | encodeDoubleOperandInstr(instr);
    case InstructionType::Sub:
        return OP_SUB | encodeDoubleOperandInstr(instr);

//    [DOR] additional 2-operand instructions require a register source operand
    case InstructionType::Mul:
        return OP_MUL | encodeDoubleOperandRegInstr(instr);
    case InstructionType::Div:
        return OP_DIV | encodeDoubleOperandRegInstr(instr);
    case InstructionType::Ash:
        return OP_ASH | encodeDoubleOperandRegInstr(instr);
    case InstructionType::Ashc:
        return OP_ASHC | encodeDoubleOperandRegInstr(instr);
    case InstructionType::Exor:
        return OP_XOR | encodeDoubleOperandRegInstr(instr);
    case InstructionType::Sob:
        return OP_SOB | encodeDoubleOperandRegInstr(instr);
    case InstructionType::Jsr:
        return OP_JSR | encodeDoubleOperandRegInstr(instr);

//    [SO] single-operand instruction
    case InstructionType::Clr:
        return OP_CLR | encodeSingleOperandInstr(instr);
    case InstructionType::Clrb:
        return OP_CLRB | encodeSingleOperandInstr(instr);
    case InstructionType::Com:
        return OP_COM | encodeSingleOperandInstr(instr);
    case InstructionType::Comb:
        return OP_COMB | encodeSingleOperandInstr(instr);
    case InstructionType::Inc:
        return OP_INC | encodeSingleOperandInstr(instr);
    case InstructionType::Incb:
        return OP_INCB | encodeSingleOperandInstr(instr);
    case InstructionType::Dec:
        return OP_DEC | encodeSingleOperandInstr(instr);
    case InstructionType::Decb:
        return OP_DECB | encodeSingleOperandInstr(instr);
    case InstructionType::Neg:
        return OP_NEG | encodeSingleOperandInstr(instr);
    case InstructionType::Negb:
        return OP_NEGB | encodeSingleOperandInstr(instr);
    case InstructionType::Tst:
        return OP_TST | encodeSingleOperandInstr(instr);
    case InstructionType::Tstb:
        return OP_TSTB | encodeSingleOperandInstr(instr);
    case InstructionType::Asr:
        return OP_ASR | encodeSingleOperandInstr(instr);
    case InstructionType::Asrb:
        return OP_ASRB | encodeSingleOperandInstr(instr);
    case InstructionType::Asl:
        return OP_ASL | encodeSingleOperandInstr(instr);
    case InstructionType::Aslb:
        return OP_ASLB | encodeSingleOperandInstr(instr);
    case InstructionType::Ror:
        return OP_ROR | encodeSingleOperandInstr(instr);
    case InstructionType::Rorb:
        return OP_RORB | encodeSingleOperandInstr(instr);
    case InstructionType::Rol:
        return OP_ROL | encodeSingleOperandInstr(instr);
    case InstructionType::Rolb:
        return OP_ROLB | encodeSingleOperandInstr(instr);
    case InstructionType::Swab:
        return OP_SWAB | encodeSingleOperandInstr(instr);
    case InstructionType::Adc:
        return OP_ADC | encodeSingleOperandInstr(instr);
    case InstructionType::Adcb:
        return OP_ADCB | encodeSingleOperandInstr(instr);
    case InstructionType::Sbc:
        return OP_SBC | encodeSingleOperandInstr(instr);
    case InstructionType::Sbcb:
        return OP_SBCB | encodeSingleOperandInstr(instr);
    case InstructionType::Sxt:
        return OP_SXT | encodeSingleOperandInstr(instr);
    case InstructionType::Jmp:
        return OP_JMP | encodeSingleOperandInstr(instr);
    case InstructionType::Mtpd:
        return OP_MTPD | encodeSingleOperandInstr(instr);
    case InstructionType::Mtpi:
        return OP_MTPI | encodeSingleOperandInstr(instr);
    case InstructionType::Mfpd:
        return OP_MFPD | encodeSingleOperandInstr(instr);
    case InstructionType::Mfpi:
        return OP_MFPI | encodeSingleOperandInstr(instr);
    case InstructionType::Mtps:
        return OP_MTPS | encodeSingleOperandInstr(instr);
    case InstructionType::Mfps:
        return OP_MFPS | encodeSingleOperandInstr(instr);
    case InstructionType::Mark:
        return OP_MARK | encodeSingleOperandInstr(instr);
    case InstructionType::Csm:
        return OP_CSM | encodeSingleOperandInstr(instr);

//    [B] condition branch instructions
    case InstructionType::Br:
        return OP_BR | instr.offset();
    case InstructionType::Bne:
        return OP_BNE | instr.offset();
    case InstructionType::Beq:
        return OP_BEQ | instr.offset();
    case InstructionType::Bpl:
        return OP_BPL | instr.offset();
    case InstructionType::Bmi:
        return OP_BMI | instr.offset();
    case InstructionType::Bvc:
        return OP_BVC | instr.offset();
    case InstructionType::Bvs:
        return OP_BVS | instr.offset();
    case InstructionType::Bcc:
        return OP_BCC | instr.offset();
    case InstructionType::Bcs:
        return OP_BCS | instr.offset();
    case InstructionType::Bge:
        return OP_BGE | instr.offset();
    case InstructionType::Blt:
        return OP_BLT | instr.offset();
    case InstructionType::Bgt:
        return OP_BGT | instr.offset();
    case InstructionType::Ble:
        return OP_BLE | instr.offset();
    case InstructionType::Bhi:
        return OP_BHI | instr.offset();
    case InstructionType::Blos:
        return OP_BLOS | instr.offset();

//    [NO] without operand instructions
    case InstructionType::Halt:
        return OP_HALT;
    case InstructionType::Wait:
        return OP_WAIT;
    case InstructionType::Reset:
        return OP_RESET;
    case InstructionType::Mfpt:
        return OP_MFPT;
    case InstructionType::Rti:
        return OP_RTI;
    case InstructionType::Rtt:
        return OP_RTT;
    case InstructionType::Iot:
        return OP_IOT;
    case InstructionType::Bpt:
        return OP_BPT;
    case InstructionType::Clc:
        return OP_CLC;
    case InstructionType::Clv:
        return OP_CLV;
    case InstructionType::Clz:
        return OP_CLZ;
    case InstructionType::Cln:
        return OP_CLN;
    case InstructionType::Ccc:
        return OP_CCC;
    case InstructionType::Sec:
        return OP_SEC;
    case InstructionType::Sev:
        return OP_SEV;
    case InstructionType::Sez:
        return OP_SEZ;
    case InstructionType::Sen:
        return OP_SEN;
    case InstructionType::Scc:
        return OP_SCC;
    default:
#ifdef __ENCODER_DEBUG__
        std::cout << "Command is not defined!" << std::endl;
#endif
        return OP_HALT;
    }
}

// TODO нужно еще добавить инструкции которые не стандартные
int16_t *pdpEncode(const Instruction *instructionList, int count)
{
    int16_t *decodedInstr = new int16_t[count];
    for (int i = 0; i < count; i++){
        decodedInstr[i] = encodeOneInstruction(*(instructionList + i));
    }
    return decodedInstr;
}
