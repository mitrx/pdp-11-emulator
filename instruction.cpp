#include "instruction.h"

InstructionOperand::InstructionOperand()
{
}

InstructionOperand::InstructionOperand(const InstructionOperand& io)
{
    m_reg = io.m_reg;
    m_mode = io.m_mode;
}

InstructionOperand& InstructionOperand::operator =(const InstructionOperand& io)
{
    m_reg = io.m_reg;
    m_mode = io.m_mode;
    return *this;
}

int16_t InstructionOperand::reg() const
{
    return m_reg;
}

AddressingMode InstructionOperand::mode() const
{
    return m_mode;
}

void InstructionOperand::setReg(const int16_t& r)
{
    m_reg = r;
}

void InstructionOperand::setMode(const AddressingMode& m)
{
    m_mode = m;
}

Instruction::Instruction()
{
    m_addr = 0;
    m_type = InstructionType::Halt;
    m_first = nullptr;
    m_second = nullptr;
    m_offset = 0;
}

Instruction::Instruction(int16_t addr, InstructionType type,
                         InstructionOperand *firstOp,
                         InstructionOperand *secondOp, int16_t offset,
                         int complexity)
{
    m_addr = addr;
    m_type = type;
    m_first = firstOp;
    m_second = secondOp;
    m_offset = offset;
    m_complexity = complexity;
}

Instruction::Instruction(const Instruction &i)
{
    m_addr = i.m_addr;
    m_type = i.m_type;
    if (i.m_first) {
        m_first = new InstructionOperand(*i.m_first);
    } else {
        m_first = nullptr;
    }
    if (i.m_second) {
        m_second = new InstructionOperand(*i.m_second);
    } else {
        m_second = nullptr;
    }
    m_offset = i.m_offset;
}

Instruction::Instruction(Instruction &&i)
{
    m_addr = i.m_addr;
    m_type = i.m_type;
    m_first = i.m_second;
    m_second = i.m_first;
    m_offset = i.m_offset;
}

Instruction& Instruction::operator =(const Instruction& i)
{
    m_addr = i.m_addr;
    m_type = i.m_type;
    if (m_first) {
        delete m_first;
    }
    m_first = nullptr;
    if (i.m_first) {
        m_first = new InstructionOperand(*i.m_first);
    }
    if (m_second) {
        delete m_second;
    }
    m_second = nullptr;
    if (i.m_second) {
        m_second = new InstructionOperand(*i.m_second);
    }
    m_offset = i.m_offset;
    return *this;
}

Instruction& Instruction::operator =(Instruction&& i)
{
    m_addr = i.m_addr;
    m_type = i.m_type;
    m_first = i.m_second;
    m_second = i.m_first;
    m_offset = i.m_offset;
    i.m_first = nullptr;
    i.m_second = nullptr;
    return *this;
}

int16_t Instruction::address() const
{
    return m_addr;
}
InstructionType Instruction::type() const
{
    return m_type;
}
InstructionOperand *Instruction::first()
{
    return m_first;
}
InstructionOperand *Instruction::second()
{
    return m_second;
}
int16_t Instruction::offset() const
{
    return m_offset;
}
int16_t Instruction::complexity() const
{
    return m_complexity;
}

std::ostream& operator<<(std::ostream& os, Instruction& i)
{
    os << "{" << std::oct << i.address() << ", " << instructionTypesMap[i.type()];
    if (i.first()) {
        os << ", {R" << i.first()->reg() << ", " <<
              addressingModesMap[i.first()->mode()] << "}";
    }
    if (i.second()) {
        os << ", {R" << i.second()->reg() << ", " <<
              addressingModesMap[i.second()->mode()] << "}";
    }
    return os << ", " << i.offset() << ", " << i.complexity() << "}";
}

Instruction::~Instruction()
{
    if (m_first) {
        delete m_first;
        m_first = nullptr;
    }
    if (m_second) {
        delete m_second;
        m_second = nullptr;
    }
}

// decoding addressing modes
AddressingMode Instruction::decodeAddrMode(int16_t opcode)
{
    //opcode 0 000 077
    if ((opcode & MASK_SECOND_REG) == R6)
    {
        return decodeStackAddrMode((opcode & MASK_SECOND_ADDR_MODE) >> 3);
    }
    else if ((opcode & MASK_SECOND_REG) == R7)
    {
        return decodeProgCountAddrMode((opcode & MASK_SECOND_ADDR_MODE) >> 3);
    }
    else
    {
        return decodeGenRegAddrMode((opcode & MASK_SECOND_ADDR_MODE) >> 3);
    }
}

// decoding [SAM] Stack Addresing Modes
AddressingMode Instruction::decodeStackAddrMode(int16_t opcode)
{
    switch (opcode) {
    case SAM_DEF:
        return AddressingMode::ST_Deferred;
        break;
    case SAM_INC:
        return AddressingMode::ST_Increment;
        break;
    case SAM_INC_DEF:
        return AddressingMode::ST_IncrementDef;
        break;
    case SAM_DEC:
        return AddressingMode::ST_Decrement;
        break;
    case SAM_IND:
        return AddressingMode::ST_Index;
        break;
    case SAM_IND_DEF:
        return AddressingMode::ST_IndexDef;
        break;
    default:
        return AddressingMode::ST_Deferred;
#ifdef __INSTRUCTION_DEBUG__
        std::cout << "this addressing mode is not implemented" << std::endl;
#endif
        break;
    }
}

// decoding [PCAM] Program Counter Addressing Modes
AddressingMode Instruction::decodeProgCountAddrMode(int16_t opcode)
{
    switch (opcode) {
    case PCAM_IMM:
        return AddressingMode::PC_Immediate;
        break;
    case PCAM_ABS:
        return AddressingMode::PC_Absolute;
        break;
    case PCAM_REL:
        return AddressingMode::PC_Relative;
        break;
    case PCAM_REL_DEF:
        return AddressingMode::PC_RelativeDef;
        break;
    default:
        return AddressingMode::PC_Immediate;
#ifdef __INSTRUCTION_DEBUG__
        std::cout << "this addressing mode is not implemented" << std::endl;
#endif
        break;
    }
}

// decoding [GRAM] General Register Addressing Modes
AddressingMode Instruction::decodeGenRegAddrMode(int16_t opcode)
{
    switch (opcode) {
    case GRAM_REG:
        return AddressingMode::GR_Register;
        break;
    case GRAM_REG_DEF:
        return AddressingMode::GR_RegisterDef;
        break;
    case GRAM_INC:
        return AddressingMode::GR_Increment;
        break;
    case GRAM_INC_DEF:
        return AddressingMode::GR_IncrementDef;
        break;
    case GRAM_DEC:
        return AddressingMode::GR_Decrement;
        break;
    case GRAM_DEC_DEF:
        return AddressingMode::GR_DecrementDef;
        break;
    case GRAM_IND:
        return AddressingMode::GR_Index;
        break;
    case GRAM_IND_DEF:
        return AddressingMode::GR_IndexDef;
        break;
    default:
#ifdef __INSTRUCTION_DEBUG__
        std::cout << "this addressing mode is not implemented" << std::endl;
#endif
        return AddressingMode::GR_Register;
        break;
    }
}

// [DO] double-operand instruction
Instruction Instruction::decodeDoubleOperandInstruction(int16_t mw){
    Instruction instr;
    switch (mw & (MASK_OPCODE_DOI)) {
    case OP_MOV:
        if (mw & MASK_BW)
            instr.m_type = InstructionType::Movb;
        else
            instr.m_type = InstructionType::Mov;
        break;
    case OP_CMP:
        if (mw & MASK_BW)
            instr.m_type = InstructionType::Cmpb;
        else
            instr.m_type = InstructionType::Cmp;
        break;
    case OP_BIT:
        if (mw & MASK_BW)
            instr.m_type = InstructionType::Bitb;
        else
            instr.m_type = InstructionType::Bit;
        break;
    case OP_BIC:
        if (mw & MASK_BW)
            instr.m_type = InstructionType::Bicb;
        else
            instr.m_type = InstructionType::Bic;
        break;
    case OP_BIS:
        if (mw & MASK_BW)
            instr.m_type = InstructionType::Bisb;
        else
            instr.m_type = InstructionType::Bis;
        break;
    case OP_ADD:
        if (mw & MASK_BW)
            instr.m_type = InstructionType::Add;
        else
            instr.m_type = InstructionType::Sub;
        break;
    default:
#ifdef __INSTRUCTION_DEBUG__
        std::cout << "Command doesn't exist! [DO]" << std::endl;
#endif
        break;
    }    
    InstructionOperand *firstOp = new InstructionOperand;
    InstructionOperand *secondOp = new InstructionOperand;
    firstOp->setReg((mw & MASK_FIRST_REG) >> 6);
    secondOp->setReg(mw & MASK_SECOND_REG);
    firstOp->setMode(decodeAddrMode((mw & MASK_FIRST_OPERAND) >> 6));
    secondOp->setMode(decodeAddrMode(mw & MASK_SECOND_OPERAND));
    instr.m_first = firstOp;
    instr.m_second = secondOp;
    return instr;
}

// [DOR] double-operand instruction with register
Instruction Instruction::decodeDoubleOperandInstructionRegister(int16_t mw){
    Instruction instr;
    switch (mw & (MASK_OPCODE_DOIR)) {
    case OP_MUL:
        instr.m_type = InstructionType::Mul;
        break;
    case OP_DIV:
        instr.m_type = InstructionType::Div;
        break;
    case OP_ASH:
        instr.m_type = InstructionType::Ash;
        break;
    case OP_XOR:
        instr.m_type = InstructionType::Exor;
        break;
    case OP_SOB:
        instr.m_type = InstructionType::Sob;
        break;
    case OP_JSR:
        instr.m_type = InstructionType::Jsr;
        break;
    default:
#ifdef __INSTRUCTION_DEBUG__
        std::cout << "Command doesn't exist! [DOR]" << std::endl;
#endif
        break;
    }
    InstructionOperand *firstOp = new InstructionOperand;
    InstructionOperand *secondOp = new InstructionOperand;
    firstOp->setReg((mw & MASK_FIRST_REG) >> 6);
    secondOp->setReg(mw & MASK_SECOND_REG);
    firstOp->setMode(AddressingMode::GR_Register);
    secondOp->setMode(decodeAddrMode(mw & MASK_SECOND_OPERAND));
    instr.m_first = firstOp;
    instr.m_second = secondOp;
    return instr;
}

// [SO] single-operand instruction
Instruction Instruction::decodeSingleOperandInstruction(int16_t mw){
    Instruction instr;
    switch (mw & (MASK_OPCODE_SOI)) {
    case OP_CLR:
        instr.m_type = InstructionType::Clr;
        break;
    case OP_CLRB:
        instr.m_type = InstructionType::Clrb;
        break;
    case OP_COM:
        instr.m_type = InstructionType::Com;
        break;
    case OP_COMB:
        instr.m_type = InstructionType::Comb;
        break;
    case OP_INC:
        instr.m_type = InstructionType::Inc;
        break;
    case OP_INCB:
        instr.m_type = InstructionType::Incb;
        break;
    case OP_DEC:
        instr.m_type = InstructionType::Dec;
        break;
    case OP_DECB:
        instr.m_type = InstructionType::Decb;
        break;
    case OP_NEG:
        instr.m_type = InstructionType::Neg;
        break;
    case OP_NEGB:
        instr.m_type = InstructionType::Negb;
        break;
    case OP_TST:
        instr.m_type = InstructionType::Tst;
        break;
    case OP_TSTB:
        instr.m_type = InstructionType::Tstb;
        break;
    case OP_ASR:
        instr.m_type = InstructionType::Asr;
        break;
    case OP_ASRB:
        instr.m_type = InstructionType::Asrb;
        break;
    case OP_ASL:
        instr.m_type = InstructionType::Asl;
        break;
    case OP_ASLB:
        instr.m_type = InstructionType::Aslb;
        break;
    case OP_ROR:
        instr.m_type = InstructionType::Ror;
        break;
    case OP_RORB:
        instr.m_type = InstructionType::Rorb;
        break;
    case OP_ROL:
        instr.m_type = InstructionType::Rol;
        break;
    case OP_ROLB:
        instr.m_type = InstructionType::Rolb;
        break;
    case OP_SWAB:
        instr.m_type = InstructionType::Swab;
        break;
    case OP_ADC:
        instr.m_type = InstructionType::Adc;
        break;
    case OP_ADCB:
        instr.m_type = InstructionType::Adcb;
        break;
    case OP_SBC:
        instr.m_type = InstructionType::Sbc;
        break;
    case OP_SBCB:
        instr.m_type = InstructionType::Sbcb;
        break;
    case OP_SXT:
        instr.m_type = InstructionType::Sxt;
        break;
    case OP_JMP:
        instr.m_type = InstructionType::Jmp;
        break;
    case OP_MTPD:
        instr.m_type = InstructionType::Mtpd;
        break;
    case OP_MTPI:
        instr.m_type = InstructionType::Mtpi;
        break;
    case OP_MFPD:
        instr.m_type = InstructionType::Mfpd;
        break;
    case OP_MFPI:
        instr.m_type = InstructionType::Mfpi;
        break;
    case OP_MTPS:
        instr.m_type = InstructionType::Mtps;
        break;
    case OP_MFPS:
        instr.m_type = InstructionType::Mfps;
        break;
    case OP_MARK:
        instr.m_type = InstructionType::Mark;
        break;
    case OP_CSM:
        instr.m_type = InstructionType::Csm;
        break;
    default:
#ifdef __INSTRUCTION_DEBUG__
        std::cout << "Command doesn't exist! [SO]" << std::endl;
#endif
        break;
    }
    InstructionOperand *firstOp = new InstructionOperand();
    firstOp->setReg(mw & MASK_SECOND_REG);
    firstOp->setMode(decodeAddrMode(mw & MASK_SECOND_OPERAND));
    instr.m_first = firstOp;
    instr.m_second = nullptr;
    return instr;
}

// [B] branch
Instruction Instruction::decodeBranchInstruction(int16_t mw){
    Instruction instr;
    switch (mw & (MASK_OPCODE_B)) {
    case OP_BR:
        instr.m_type = InstructionType::Br;
        break;
    case OP_BNE:
        instr.m_type = InstructionType::Bne;
        break;
    case OP_BEQ:
        instr.m_type = InstructionType::Beq;
        break;
    case OP_BPL:
        instr.m_type = InstructionType::Bpl;
        break;
    case OP_BMI:
        instr.m_type = InstructionType::Bmi;
        break;
    case OP_BVC:
        instr.m_type = InstructionType::Bvc;
        break;
    case OP_BVS:
        instr.m_type = InstructionType::Bvs;
        break;
    case OP_BCC:
        instr.m_type = InstructionType::Bcc;
        break;
    case OP_BCS:
        instr.m_type = InstructionType::Bcs;
        break;
    case OP_BGE:
        instr.m_type = InstructionType::Bge;
        break;
    case OP_BLT:
        instr.m_type = InstructionType::Blt;
        break;
    case OP_BGT:
        instr.m_type = InstructionType::Bgt;
        break;
    case OP_BLE:
        instr.m_type = InstructionType::Ble;
        break;
    case OP_BHI:
        instr.m_type = InstructionType::Bhi;
        break;
    case OP_BLOS:
        instr.m_type = InstructionType::Blos;
        break;
//    case OP_BHIS:
//        instr.m_type = InstructionType::Bhis;
//        break;
//    case OP_BLO:
//        instr.m_type = InstructionType::Blo;
//        break;
    default:
#ifdef __INSTRUCTION_DEBUG__
        std::cout << "Command doesn't exist! [B]" << std::endl;
#endif
        break;
    }
    instr.m_first = nullptr;
    instr.m_second = nullptr;
    instr.m_offset = mw & MASK_OFFSET_B;
    return instr;
}

// [NO] without operand instruction
Instruction Instruction::decodeNoOperandInstruction(int16_t mw){
    Instruction instr;
    switch (mw) {
    case OP_HALT:
        instr.m_type = InstructionType::Halt;
        break;
    case OP_WAIT:
        instr.m_type = InstructionType::Wait;
        break;
    case OP_RESET:
        instr.m_type = InstructionType::Reset;
        break;
    case OP_MFPT:
        instr.m_type = InstructionType::Mfpt;
        break;
    case OP_RTI:
        instr.m_type = InstructionType::Rti;
        break;
    case OP_RTT:
        instr.m_type = InstructionType::Rtt;
        break;
    case OP_IOT:
        instr.m_type = InstructionType::Iot;
        break;
    case OP_BPT:
        instr.m_type = InstructionType::Bpt;
        break;
    default:
#ifdef __INSTRUCTION_DEBUG__
        std::cout << "Command doesn't exist! [NO]" << std::endl;
#endif
        break;
    }
    instr.m_first = nullptr;
    instr.m_second = nullptr;
    return instr;
}

// [CC] condition code operation
Instruction Instruction::decodeConditionCode(int16_t mw)
{
    Instruction instr;
    switch (mw) {
    case OP_CLC:
        instr.m_type = InstructionType::Clc;
        break;
    case OP_CLV:
        instr.m_type = InstructionType::Clv;
        break;
    case OP_CLZ:
        instr.m_type = InstructionType::Clz;
        break;
    case OP_CLN:
        instr.m_type = InstructionType::Cln;
        break;
    case OP_CCC:
        instr.m_type = InstructionType::Ccc;
        break;
    case OP_SEC:
        instr.m_type = InstructionType::Sec;
        break;
    case OP_SEV:
        instr.m_type = InstructionType::Sev;
        break;
    case OP_SEZ:
        instr.m_type = InstructionType::Sez;
        break;
    case OP_SEN:
        instr.m_type = InstructionType::Sen;
        break;
    case OP_SCC:
        instr.m_type = InstructionType::Scc;
        break;
    default:
#ifdef __INSTRUCTION_DEBUG__
        std::cout << "Command doesn't exist! [CC]" << std::endl;
#endif
        break;
    }
    instr.m_first = nullptr;
    instr.m_second = nullptr;
    return instr;
}

Instruction Instruction::decode(int16_t mw)
{
    /*
    int16_t *intInstrStream = (int16_t *) instrStream;

    // machine word
    int16_t mw = *intInstrStream;
    intInstrStream++;

    // replaced x and y with mw
    mw = (((int16_t) mw) << 8) | (((int16_t) mw) & 0x00FF);
    */

    // double-operand instruction with register
    if ((mw & (MASK_DOIR)) == (VALUE_DOIR))
    {
        return decodeDoubleOperandInstruction(mw);
    }
    // double-operand instruction
    else if ((mw & MASK_OPCODE_DOI) != 0)
    {
        return decodeDoubleOperandInstruction(mw);
    }
    // single-operand instruction
    else if ((mw & (MASK_SOI)) == (VALUE_SOI))
    {
        return decodeSingleOperandInstruction(mw);
    }
    // branch
    else if ((mw & (MASK_B)) != 0)
    {
        return decodeBranchInstruction(mw);
    }
    // without operand instruction
    // !! temporary
    else
    {
        if ((mw & ((int16_t)0177770)) == 0)
            return decodeNoOperandInstruction(mw);
        else
            return decodeConditionCode(mw);
    }
}
