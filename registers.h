#ifndef REGISTERS_H
#define REGISTERS_H

#include "config.h"
#include <exception>
#define REGISTERS_NUMBER 9 // 8 registers + processor status word
#define MASK(x) (1<<(x))

class InvalidRegisterIndexException: std::exception
{
    virtual const char* what() const throw()
    {
      return "No register with specified index";
    }
};

class Registers
{
public:
    Registers();
    ~Registers();
    void reset();
    void setR0(uint16_t val);
    void setR1(uint16_t val);
    void setR2(uint16_t val);
    void setR3(uint16_t val);
    void setR4(uint16_t val);
    void setR5(uint16_t val);
    void setSp(uint16_t val);
    void setPc(uint16_t val);
    void setPsw(uint16_t val);
    void setReg(uint16_t number, uint16_t val);
    int16_t getReg(uint16_t number);
    void setN();
    void setZ();
    void setV();
    void setC();
    void clearN();
    void clearZ();
    void clearV();
    void clearC();
    bool getN();
    bool getZ();
    bool getV();
    bool getC();
    uint16_t r0();
    uint16_t r1();
    uint16_t r2();
    uint16_t r3();
    uint16_t r4();
    uint16_t r5();
    uint16_t sp();   // R6 is a stack pointer
    uint16_t pc();   // R7 is a program counter
    uint16_t psw();  // R9 is a processor status word
    uint16_t registerAt(int pos);
    void setRegisterAt(int pos, uint16_t value);

private:
    uint16_t *m_registers;
    InvalidRegisterIndexException invalidRegEx;
};

#endif // REGISTERS_H
