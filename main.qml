import QtQuick 2.7
import QtQuick.Window 2.0
import QtQuick.Controls 2.0

Window {
    visible: true
    width: 630
    height: 500

    MainForm {
        objectName: "mainForm"
        width: parent.width
        height: parent.height

        disasmText.text: disassembler.text
        regsText.text: registers.text
        hexvText.text: hexview.text

        monitorImage {
            source: monitor.source
        }

        buttonRun {
            onClicked: {
                emulator.run();
            }
        }

        buttonStep {
            onClicked: {
                emulator.step();
            }
        }

        buttonReset {
            onClicked: {
                emulator.reset();
            }
        }
    }
}
