#ifndef HEXVIEWERVIEW_H
#define HEXVIEWERVIEW_H

#include <QObject>
#include <string>
#include <sstream>
#include <iostream>
#include "emulator.h"
#include "memory.h"

class HexViewerView : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString text READ text NOTIFY textChanged)
public:
    explicit HexViewerView(Emulator& emulator, QObject *parent = 0);
    ~HexViewerView();
    Q_INVOKABLE void render();
    QString text();
signals:
    void textChanged();
private:
    Memory& m_memory;
    QString *m_text;
};

#endif // HEXVIEWERVIEW_H
