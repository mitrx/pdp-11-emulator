#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQmlEngine>
#include <QQmlComponent>
#include <QObject>
#include <QDebug>
#include "disassemblerview.h"
#include "registersview.h"
#include "hexviewerview.h"
#include "monitorview.h"
#include "monitorcontroller.h"
#include "emulator.h"
#include "emulatorcontroller.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;

    // Emulator
    Emulator *emulator = new Emulator();
    emulator->initialize();
    EmulatorController *emulatorController = new EmulatorController(*emulator, &app);
    engine.rootContext()->setContextProperty("emulator", emulatorController);
    // Registers
    RegistersView *regsView = new RegistersView(*emulator, &app);
    engine.rootContext()->setContextProperty("registers", regsView);
    // Disassembler
    DisassemblerView *disasmView = new DisassemblerView(*emulator, &app);
    engine.rootContext()->setContextProperty("disassembler", disasmView);
    // Hex Viewer
    HexViewerView *hexvView = new HexViewerView(*emulator, &app);
    engine.rootContext()->setContextProperty("hexview", hexvView);
    // Image and Painter
    MonitorView *monitorView = new MonitorView(*emulator, &app);
    MonitorController *monitorController = new MonitorController(*monitorView, &app);
    engine.addImageProvider("monitorImageProvider", monitorView);
    engine.rootContext()->setContextProperty("monitor", monitorController);

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    return app.exec();
}
