#include "hexviewerview.h"

HexViewerView::HexViewerView(Emulator& emulator, QObject *parent)
    : QObject(parent), m_memory(emulator.memory()), m_text(new QString())
{
}

HexViewerView::~HexViewerView()
{
    delete m_text;
}

QString HexViewerView::text()
{
    return *m_text;
}

void HexViewerView::render()
{
    std::stringstream sstream;
    sstream << "dsdfias";
    m_text = new QString(QString::fromStdString(sstream.str()));
    emit textChanged();
}
