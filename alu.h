#ifndef ALU_H
#define ALU_H

#include "config.h"
#include "registers.h"
#include "memory.h"
#include "limits.h"

#define MPI  ((int16_t) 0077777) // Most Positive Integer (Word)
#define MPIB ((int8_t) 0177)     // Most Positive Integer (Byte)
#define MNI  ((int16_t) 0100000) // Most Negative Integer (Word)
#define MNIB ((int8_t) 0200)     // Most Negative Integer (Byte)

class Alu
{
public:
    Alu(Registers& regs, Memory& mem);
    int16_t adc(int16_t x);             // [ADC] Add Carry (word)
    int8_t adcb(int8_t x);              // [ADCB]Add Carry (byte)
    int16_t add(int16_t x, int16_t y);  // [ADD]
    int16_t ash(int16_t x, int16_t y);  // [ASH] Arithmetic shift
    int32_t ashc(int16_t x, int16_t y, int16_t z); // [ASHC] Arithmetic shift combined
    int16_t asl(int16_t x);             // [ASL] Arithmetic shift left
    int8_t aslb(int8_t x);              // [ASLB] Arithmetic shift left (byte)
    int16_t asr(int16_t x);             // [ASR] Arithmetic shift right
    int8_t asrb(int8_t x);              // [ASR] Arithmetic shift right byte
    int16_t clr();                      // [CLR] Clear
    int8_t clrb();                      // [CLRB]
    void cmp(int16_t x, int16_t y);     // [CMP] Compare
    void cmpb(int8_t x, int8_t y);      // [CMPB]
    int16_t com(int16_t x);             // [COM] Complement
    int8_t comb(int8_t x);              // [COMB]
    int16_t dec(int16_t x);             // [DEC] Decrement
    int8_t decb(int8_t x);              // [DECB]
    int32_t div(int16_t r, int16_t r1, int16_t src);  // [DIV] Division
    int16_t inc(int16_t x);             // [INC] Imcrement
    int8_t incb(int8_t x);              // [INCB]
    void jmp(int16_t x);                // [JMP] Jump
    void jsr(int16_t reg, int16_t dst); // [JSR] Jump to subroutine
    int16_t mov(int16_t x, int16_t y);  // [MOV] Move
    int8_t movb(int8_t x, int8_t y);    // [MOVB]
    int32_t mul(int16_t r, int16_t src);// [MUL] Multiply
    int16_t neg(int16_t x);             // [NEG] Negate
    int8_t negb(int8_t x);              // [NEGB]
    int16_t rol(int16_t x);             // [ROL] Rotate left
    int8_t rolb(int8_t x);              // [ROLB]
    int16_t ror(int16_t x);             // [ROR] Rotate right
    int8_t rorb(int8_t x);              // [RORB]
    int16_t sbc(int16_t x);             // [SBC] Subtract Carry
    int8_t sbcb(int8_t x);              // [SBCB]
    int16_t sub(int16_t x, int16_t y);  // [SUB] Subtract
    int16_t swab(int16_t x);            // [SWAB] Swap Bytes
    int16_t sxt();                      // [SXT] Sign Extend
    int16_t exor(int16_t x, int16_t y); // [XOR] Exclusive OR
    int16_t bic(int16_t x, int16_t y);  // [BIC] bit clear
    int8_t bicb(int8_t x, int8_t y);    // [BICB]
    int16_t bis(int16_t x, int16_t y);  // [BIS] bit set
    int8_t bisb(int8_t x, int8_t y);    // [BISB]
    int16_t bit(int16_t x, int16_t y);  // [BIT] bit test
    int8_t bitb(int8_t x, int8_t y);    // [BITB]
    // BRANCH
    void bcc(int16_t offset);           // [BCC] if carry clear
    void bcs(int16_t offset);           // [BCS] if carry set
    void beq(int16_t offset);           // [BEQ] if zero
    void bge(int16_t offset);           // [BGE] if greater than or equal
    void bgt(int16_t offset);           // [BGT] if greater than
    void bhi(int16_t offset);           // [BHI] if higher
    void ble(int16_t offset);           // [BLE] if lesss than or equal
    void blos(int16_t offset);          // [BLOS] if lower or same
    void blt(int16_t offset);           // [BLT] if less than
    void bmi(int16_t offset);           // [BMI] if minus
    void bne(int16_t offset);           // [BNE] if not equal
    void bpl(int16_t offset);           // [BPL] if plus
    void br(int16_t offset);            // [BR] uncondition
    void bvc(int16_t offset);           // [BVC] if V bit clear
    void bvs(int16_t offset);           // [BVS] if V bit set
    //
    void emt();                         // [EMT] Emulator trap
    void iot();                         // [IOT] I/O trap
    void bpt();                         // [BPT] Breakpoint trap
    void rti();                         // [RTI] Return from interrupt
    int16_t rts(int16_t reg);           // [RTS] Return from subroutine
    void rtt();                         // [RTT] Return from interrupt
    void trap();                        // [TRAP] trap
    void mark(int16_t N);               // [MARK]
    int16_t mfps();                     // [MFPS] move byte from PSW
    void mfpd(int16_t src);             // [MFPD] move from previouse, mfpi
    int16_t mtpd();                     // [MTPD] move to previouse data space, mtpi
    void mtps(int16_t src);             // [MTPS] move byte to PSW
    // MFPT
    // MNS
    // MPP


    // Condition codes
    void ccc(); // Clear all
    void clc(); // Clear bit C
    void cln(); // Clear bit N
    void clv(); // Clear bit V
    void clz(); // Clear bit Z
    void scc(); // Set all
    void sec(); // Set bit C
    void sen(); // Set bit N
    void sev(); // Set bit V
    void sez(); // Set bit Z



private:
    void setNZ(int16_t result);
    void setNZb(int8_t result);
    void pushStack(int16_t val);
    int16_t popStack();
    void loadIvtPc(int16_t addr);
    void loadIvtPs(int16_t addr);
    Registers& m_registers;
    Memory& m_memory;
};


#endif // ALU_H
