#include "pipeline.h"

#include "pipelinestate.h"

Pipeline::Pipeline(Memory& memory, Registers& registers, Alu& alu)
    : m_memory(memory), m_registers(registers), m_alu(alu)
{
}

bool Pipeline::cycle()
{
    fetchStage();
    decodeStage();
    operandsFetchStage();
    executeStage();
    writeBackStage();
    curr = next;
    return curr.halt;
}

void Pipeline::fetchStage()
{
    if (curr.fetchCycles > 0) {
        next.fetchCycles--;
#ifdef __PIPELINE_DEBUG__
        std::cout << "Instruction at address \"" << m_registers.pc() << "\" is fetching, "
                  << std::oct << curr.fetchCycles << " cycles left" << std::endl;
#endif
    }
    if (curr.fetchFinished == true) {
#ifdef __PIPELINE_DEBUG__
        std::cout << "Don't fetch the next instruction until the previous" <<
                     " one hasn't passed to decoding phase" << std::endl;
#endif
        // decoder hasn't grabed the fetched instruction yet
        // don't fetch the next instruction until the currently
        // fetched is not passed to the decode phase
        return;
    }
    if (next.fetchCycles == 0) {
#ifdef __PIPELINE_DEBUG__
        std::cout << "instruction at address \"" << std::oct
                  << m_registers.pc() << "\" is fetched" << std::endl;
#endif
        fetch();
        if (m_registers.pc() + MW_LENGTH > MEMORY_AMOUNT) {
            std::cout << m_registers.pc() << " " << MEMORY_AMOUNT << std::endl;
            std::cout << "PC reached the end of memory" << std::endl
                      << "HALTING!!!" << std::endl;
            next.halt = true;
        }
        // move counter to the next word
        m_registers.setPc(m_registers.pc() + MW_LENGTH);    // + machine word length
        next.fetchFinished = true;
#ifdef __PIPELINE_DEBUG__
        std::cout << "starting to fetch instruction at \"" << std::oct <<
                     m_registers.pc() << "\"" << std::endl;
#endif
        // assume, we always fetch instructions from memory
        next.fetchCycles = MEMORY_ACCESS_DURATION / CYCLE_DURATION; // 1 mcs to fetch a word from memory
        return;
    }
}

void Pipeline::fetch()
{
    next.fetchedInstruction = m_memory.getWord(m_registers.pc());
}

void Pipeline::decodeStage()
{
    if (curr.decodeFinished == true) {
#ifdef __PIPELINE_DEBUG__
        std::cout << "No instruction to decode" << std::endl;
#endif
        // operand fetcher hasn't grabed the decoded instruction yet
        // we don't go to the next phase until the decoded instruction
        // is passed to the next phase
        return;
    }
    if (curr.decodeCycles > 0) {
        next.decodeCycles--;
    }
    if (next.decodeCycles == 0) {
        next.decodeCycles--;
        decode();
        next.decodeFinished = true;
#ifdef __PIPELINE_DEBUG__
        std::cout << "Instruction decoded = " << next.decodedInstruction
                  << std::endl;
#endif
    }
    if (curr.decodeCycles <= 0 && next.fetchFinished) {
        next.decodingInstuction = curr.fetchedInstruction;
        next.fetchFinished = false;
        next.decodeCycles = 1; // assume decode lasts 1 cpu cycle
    }
}

void Pipeline::resetFetch()
{
    next.fetchFinished = false;
    next.fetchCycles = 0;
}

void Pipeline::decode()
{
    int16_t fetchedInstruction = curr.fetchedInstruction;
    Instruction decodedInstruction = Instruction::decode(fetchedInstruction);
    next.decodedInstruction = decodedInstruction;
}

void Pipeline::resetDecode()
{
    next.decodeFinished = false;
}

void Pipeline::operandsFetchStage()
{
    if (curr.operandFetchFinished == true) {
        // operands haven't passed to the execution stage
        return;
    }
    if (curr.operandFetchCycles > 0) {
        next.operandFetchCycles--;
#ifdef __PIPELINE_DEBUG__
        std::cout << "Operands fetch cycles left: "
                  << next.operandFetchCycles << std::endl;
#endif
    }
    if (next.operandFetchCycles == 0) {
        next.operandFetchCycles--;
        operandsFetch();
#ifdef __PIPELINE_DEBUG__
        std::cout << "Operands of instruction "
                  << curr.operandFetchingInstruction
                  << " are (" << next.operandFetchFirst << ", "
                  << next.operandFetchSecond << ")" << std::endl;
#endif
        next.operandFetchFinished = true;
    }
    if (curr.operandFetchCycles <= 0 && next.decodeFinished) {
        next.operandFetchingInstruction = next.decodedInstruction;
        next.decodeFinished = false;
        next.operandFetchCycles = 0;
        if (next.operandFetchingInstruction.first()) {
            next.operandFetchCycles = opFetchComplexity(
                    *next.operandFetchingInstruction.first());
        }
        if (next.operandFetchingInstruction.second()) {
            next.operandFetchCycles += opFetchComplexity(
                    *next.operandFetchingInstruction.second());
        }
    }
}

void Pipeline::operandsFetch()
{
    // make actual operands fetch
    // set next.operandFetchFirst
    // set next.operandFetchSecond
    if (curr.operandFetchingInstruction.first()) {
        curr.operandFetchFirst = operFetch(curr.operandFetchingInstruction.first());
    }
    if (curr.operandFetchingInstruction.second()) {
        curr.operandFetchSecond = operFetch(curr.operandFetchingInstruction.second());
    }
}

int16_t Pipeline::operFetch(InstructionOperand *operand)
{
    int16_t tmp = 0;
    int16_t nextWord = 0; // is taken from a second word of the instruction
    switch (operand->mode()) {
    case AddressingMode::GR_Register:
        return m_registers.getReg(operand->reg());
    case AddressingMode::GR_RegisterDef:
        return m_memory.getWord(m_registers.getReg(operand->reg()));
    case AddressingMode::GR_Increment:
        tmp = m_memory.getWord(m_registers.getReg(operand->reg()));
        return tmp;
    case AddressingMode::GR_IncrementDef:
        tmp = m_memory.getWord(m_memory.getWord(m_registers.getReg(operand->reg())));
        return tmp;
    case AddressingMode::GR_Decrement:
        m_registers.setReg(operand->reg(), m_registers.getReg(operand->reg()) - 2);
        return m_memory.getWord(m_registers.getReg(operand->reg()));
    case AddressingMode::GR_DecrementDef:
        m_registers.setReg(operand->reg(), m_registers.getReg(operand->reg()) - 4);
        return m_memory.getWord(m_memory.getWord(m_registers.getReg(operand->reg())));
    case AddressingMode::GR_Index:
//        nextWord = fetchNextWord();
        return m_memory.getWord(nextWord + m_registers.getReg(operand->reg()));
    case AddressingMode::GR_IndexDef:
//        nextword = fetchNextWord();
        return m_memory.getWord(m_memory.getWord(nextWord + m_registers.getReg(operand->reg())));
    case AddressingMode::PC_Immediate:
//        nextWord = fetchNextWord();
        return nextWord;
    case AddressingMode::PC_Absolute:
        return m_memory.getWord(nextWord);
    case AddressingMode::PC_Relative:
//        nextWord = fetchNextWord();
        tmp = m_memory.getWord(m_registers.pc() + nextWord);
        return tmp;
    case AddressingMode::PC_RelativeDef:
//        nextWord = fetchNextWord();
        tmp = m_memory.getWord(m_registers.pc() + nextWord);
        m_registers.setPc(m_registers.pc() + 2);
        return m_memory.getWord(tmp);
    case AddressingMode::ST_Deferred:
        return m_memory.getWord(m_registers.sp());
    case AddressingMode::ST_Increment:
        return m_memory.getWord(m_registers.sp());
    case AddressingMode::ST_IncrementDef:
        return m_memory.getWord(m_memory.getWord(m_registers.sp()));
    case AddressingMode::ST_Decrement:
//      push value onto the stack
        return m_memory.getWord(operand->reg());
        m_registers.setSp(m_registers.sp() + 2);
        m_memory.writeRam(m_registers.sp(), tmp);
    case AddressingMode::ST_Index:
//        nextWord = fetchNextWord();
        return m_memory.getWord(m_registers.sp() - nextWord);
    case AddressingMode::ST_IndexDef:
//        nextWord = fetchNextWord();
        return m_memory.getWord(m_memory.getWord(m_registers.sp() - nextWord));
    default:
        return 0;
    }
}

int Pipeline::opFetchComplexity(InstructionOperand io)
{
    // calculate operand fetch complexity based on mode and its contents
    return 1;
}

void Pipeline::resetOperandFetch()
{
    next.operandFetchCycles = 0;
    next.operandFetchFinished = false;
}

void Pipeline::executeStage()
{
    if (curr.executionFinished == true) {
        // execution result hasn't passed to the write back stage yet
        return;
    }
    if (curr.executionCycles > 0) {
        next.executionCycles--;
    }
    if (next.executionCycles == 0) {
        next.executionCycles--;
        uint16_t oldPc = m_registers.pc();
        execute();
        next.executionFinished = true;
#ifdef __PIPELINE_DEBUG__
        std::cout << "Instruction " << curr.executingInstruction
                  << " with parameters (" << curr.executingFirst
                  << ", " << curr.executingSecond
                  << ") is EXECUTED with result "
                  << curr.executionResult << std::endl;
#endif
        // if PC was changed we need to reset all the previous stages
        // or else we will execute wrong instructions
        if (oldPc != m_registers.pc()) {
            resetFetch();
            resetDecode();
            resetOperandFetch();
        }
    }
    if (curr.executionCycles <= 0 && next.operandFetchFinished) {
        next.executingInstruction = next.operandFetchingInstruction;
        next.executingFirst = next.operandFetchFirst;
        next.executingSecond = next.operandFetchSecond;
        next.operandFetchFinished = false;
        next.executionCycles = next.executingInstruction.complexity();
    }
}

void Pipeline::execute()
{
    // make actual execution of the instruction
    // set execution result to next.executionResult
    if (curr.executingInstruction.type() == InstructionType::Halt) {
        next.halt = true;
        std::cout << "HALTING!!!" << std::endl;
    }
}

int Pipeline::execComplexity(Instruction i)
{
    // calculate write back complexity based on where do we write the result
    return 1;
}

void Pipeline::writeBackStage()
{
    if (curr.writeBackCycles > 0) {
        next.writeBackCycles--;
    }
    if (next.writeBackCycles == 0) {
        next.writeBackCycles--;
        writeBack();
#ifdef __PIPELINE_DEBUG__
        std::cout << "Instruction " << curr.executedInstruction
                  << " is WRITTEN BACK" << std::endl;
#endif
        next.completedCount++;
    }
    if (curr.writeBackCycles <= 0 && next.executionFinished) {
        next.writeBackResult = next.executionResult;
        next.executedInstruction = next.executingInstruction;
        next.executionFinished = false;
        next.writeBackCycles = wbComplexity(next.executedInstruction);
    }
}

// write back writes back the current instruction and takes the new one to wirte back
void Pipeline::writeBack()
{
    // write back what is in curr.writeBackResult
    if (curr.executedInstruction.second() != nullptr) {
        writeB(curr.writeBackResult, curr.executedInstruction.first());
    } else if (curr.executedInstruction.first() != nullptr) {
        writeB(curr.writeBackResult, curr.executedInstruction.second());
    }

}

void Pipeline::writeB(int16_t result, InstructionOperand *operand)
{
    int16_t nextWord = 0; // fetch from  second instruction
    switch (operand->mode()) {
    case AddressingMode::GR_Register:
        m_registers.setReg(operand->reg(), result);
        break;
    case AddressingMode::GR_RegisterDef:
        m_memory.writeWord(m_registers.getReg(operand->reg()), result);
        break;
    case AddressingMode::GR_Increment:
        m_memory.writeWord(m_registers.getReg(operand->reg()), result);
        m_registers.setReg(operand->reg(), m_registers.getReg(operand->reg()) + 2);
        break;
    case AddressingMode::GR_IncrementDef:
        m_memory.writeWord(m_memory.getWord(m_registers.getReg(operand->reg())), result);
        m_registers.setReg(operand->reg(), m_registers.getReg(operand->reg()) + 4);
        break;
    case AddressingMode::GR_Decrement:
        m_memory.writeWord(m_registers.getReg(operand->reg()), result);
        break;
    case AddressingMode::GR_DecrementDef:
        m_memory.writeWord(m_memory.getWord(m_registers.getReg(operand->reg())), result);
        break;
    case AddressingMode::GR_Index:
//        nextWord = fetchNextWord();
        m_memory.writeWord(nextWord + m_registers.getReg(operand->reg()), result);
        break;
    case AddressingMode::GR_IndexDef:
//        nextword = fetchNextWord();
        m_memory.writeWord(m_memory.getWord(nextWord + m_registers.getReg(operand->reg())), result);
        break;
    case AddressingMode::PC_Immediate:
        break;
    case AddressingMode::PC_Absolute:
//        nextWord = fetchNextWord();
        m_memory.writeWord(nextWord, result);
        break;
    case AddressingMode::PC_Relative:
//        nextWord = fetchNextWord();
        m_memory.writeWord(m_registers.pc() + nextWord, result);
        m_registers.setPc(m_registers.pc() + 2);
        break;
    case AddressingMode::PC_RelativeDef:
//        nextWord = fetchNextWord();
        m_memory.writeWord(m_memory.getWord(m_registers.pc() + nextWord), result);
        m_registers.setPc(m_registers.pc() + 2);
        break;
    case AddressingMode::ST_Deferred:
        m_memory.writeWord(m_registers.sp(), result);
        break;
    case AddressingMode::ST_Increment:
        m_memory.writeWord(m_registers.sp(), result);
        m_registers.setSp(m_registers.sp() - 2);
        break;
    case AddressingMode::ST_IncrementDef:
        m_memory.writeWord(m_memory.getWord(m_registers.sp()), result);
        m_registers.setSp(m_registers.sp() - 2);
        break;
    case AddressingMode::ST_Decrement:
//      push value onto the stack
        m_registers.setSp(m_registers.sp() + 2);
        m_memory.writeRam(m_registers.sp(), result);
        break;
    case AddressingMode::ST_Index:
//        nextWord = fetchNextWord();
        m_memory.writeWord(m_registers.sp() - nextWord, result);
        break;
    case AddressingMode::ST_IndexDef:
//        nextWord = fetchNextWord();
        m_memory.writeWord(m_memory.getWord(m_registers.sp() - nextWord), result);
        break;
    default:
        break;
    }
}

int Pipeline::wbComplexity(Instruction i)
{
    // calculate write back complexity based on where do we write the result
    return 1;
}
