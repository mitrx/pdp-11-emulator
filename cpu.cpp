#include "cpu.h"

SlowCpuException slowCpuException;

Cpu::Cpu(Memory &memory)
    : m_memory(memory),
      m_registers(new Registers),
      m_cycles(0)
{
    m_alu = new Alu(*m_registers, m_memory);
    m_pipeline = new Pipeline(memory, *m_registers, *m_alu);
}

void Cpu::initialize()
{
    m_cycles = 0;
    // TODO: implement
    // Description: execute program from ROM which
    // 1. Loads the program from ROM to RAM
    // 2. Copies font from ROM to RAM
    // 3. Copies program logo from ROM to RAM (?? not sure)
    // 4. Set Program Counter (R7) to the begining of rom
    m_registers->setPc(m_memory.getRomStart());
}

void Cpu::reset()
{
    m_registers->reset();
    m_registers->setPc(m_memory.getRomStart());
    m_cycles = 0;
    delete m_pipeline; 
    m_pipeline = new Pipeline(m_memory, *m_registers, *m_alu);
}

int Cpu::frequency()
{
    return CPU_FREQUENCY;
}

size_t Cpu::cycles()
{
    return m_cycles;
}

bool Cpu::cycle()
{
    bool result = m_pipeline->cycle();
    m_cycles++;
    return result;
}

Registers& Cpu::registers()
{
    return *m_registers;
}

Cpu::~Cpu()
{
    delete m_pipeline;
    delete m_registers;
    delete m_alu;
}
