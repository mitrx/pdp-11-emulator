#include "monitorcontroller.h"

MonitorController::MonitorController(MonitorView& view, QObject *parent)
    : QObject(parent), m_view(view)
{
    m_source = "image://monitorImageProvider/monitor.png";
    pictureCount = 0;
}

void MonitorController::fillPictureWithRed()
{
    m_view.fillRed();
}

void MonitorController::render()
{
    m_view.render();
    m_source = QString("image://monitorImageProvider/monitor" + QString::number(pictureCount++) + ".png");
    emit sourceChanged();
}

QString MonitorController::source()
{
    return m_source;
}
