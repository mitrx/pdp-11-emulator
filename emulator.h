#ifndef EMULATOR_H
#define EMULATOR_H

#include "config.h"
#include <thread>
#include <chrono>
#include "cpu.h"
#include "memory.h"

class Emulator
{
public:
    Emulator();
    ~Emulator();
    void initialize();
    void run();
    void step();
    void reset();
    void stop();
    Cpu& cpu();
    Memory& memory();
private:
    static void runLoop(Emulator *self);
    static void trackLoop(Emulator *self);
    Cpu *m_cpu;
    Memory *m_memory;
    bool m_initialized;
    bool m_finished;
    bool m_stop;
    bool m_wait;
    std::chrono::nanoseconds m_sleepAmount;
};

#endif // EMULATOR_H
