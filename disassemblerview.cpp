#include "disassemblerview.h"
#include <string>
#include <sstream>
#include <iostream>
#include <QDebug>

DisassemblerView::DisassemblerView(Emulator& emulator, QObject *parent)
    : QObject(parent), m_memory(emulator.memory()), m_text(new QString())
{
}

void DisassemblerView::render()
{
    std::stringstream sstream;
//    for (auto instr: instructionsVect) {
////        sstream << std::hex << instr.address() << "   ";
////        sstream << instr.instruction() << "  " << instr.firstOperand();
////        sstream << (instr.secondOperand() == "" ? "" : ",");
////        sstream << instr.secondOperand() << '\n';
//    }

    QString *newTextPtr = new QString(QString::fromStdString(sstream.str()));
    if (*newTextPtr != *m_text) {
        delete m_text;
        m_text = newTextPtr;
        emit textChanged();
    }
}

QString DisassemblerView::text() const
{
    return *m_text;
}

DisassemblerView::~DisassemblerView()
{
    delete m_text;
}
