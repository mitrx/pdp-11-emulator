#ifndef MEMORYCHUNK_H
#define MEMORYCHUNK_H

#include <array>
#include <cstdint>

class MemoryChunk
{
public:
    MemoryChunk(int16_t address, std::array<int8_t, 16> memory);
    MemoryChunk(const MemoryChunk& c);
    MemoryChunk& operator=(const MemoryChunk& c);
    MemoryChunk(MemoryChunk&& c);
    MemoryChunk& operator=(MemoryChunk&& c);
    // access
    int16_t address() const;
    const std::array<int8_t, 16>& memory() const;
private:
    int16_t m_address;
    std::array<int8_t, 16>  m_memory;
};

#endif // MEMORYCHUNK_H
