#ifndef PIPELINE_H
#define PIPELINE_H

#include "config.h"
#include "memory.h"
#include "registers.h"
#include "pipelinestate.h"
#include "instruction.h"
#include "alu.h"

class Pipeline
{
public:
    Pipeline(Memory& memory, Registers& registers, Alu& alu);
    bool cycle();
private:
    void fetchStage();
    void resetFetch();
    void fetch();
    void decodeStage();
    void resetDecode();
    void decode();
    void operandsFetchStage();
    void resetOperandFetch();
    void operandsFetch();
    void executeStage();
    void execute();
    void writeBackStage();
    void writeBack();
    int opFetchComplexity(InstructionOperand io);
    int wbComplexity(Instruction i);
    int execComplexity(Instruction i);

    int16_t operFetch(InstructionOperand *operand);
    void writeB(int16_t result, InstructionOperand *operand);

    Memory &m_memory;
    Registers &m_registers;
    Alu &m_alu;
    PipelineState curr;
    PipelineState next;
};

#endif // PIPELINE_H
