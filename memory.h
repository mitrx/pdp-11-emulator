#ifndef MEMORY_H
#define MEMORY_H

#include "config.h"

#define MEMORY_AMOUNT (64 * 1024)           // 64KByte of memory + 8 16-bit registers + process status word
#define RAM_START   ((uint16_t) 0)
#define RAM_LIMIT   ((uint16_t) (RAM_START + 16 * 1024))
#define VRAM_START  ((uint16_t) (RAM_START + RAM_LIMIT))
#define VRAM_LIMIT  ((uint16_t) (VRAM_START + 32 * 1024))
#define ROM_START   ((uint16_t) VRAM_LIMIT)
#define ROM_LIMIT   ((uint16_t) 0160000)
#define IO_START    ((uint16_t) 0160000)
#define IO_LIMIT    ((uint16_t) 0177777)

class InvalidMemoryAddressException: std::exception
{
    virtual const char* what() const throw()
    {
        return "No register with specified index";
    }
};

class Memory
{
public:
    Memory();
    ~Memory();
    uint16_t ivtAddr(uint16_t addr);
    uint16_t ramAddr(uint16_t addr);
    uint16_t romAddr(uint16_t addr);
    uint16_t vramAddr(uint16_t addr);
    uint16_t ioAddr(uint16_t addr);
    uint16_t getWord(uint16_t addr);
    uint16_t getRomStart();
    void initialize();
    void clear();
    void setIvtLimit(uint16_t limit);
    bool writeRam(uint16_t addr, uint16_t value);
    bool writeVram(uint16_t addr, uint16_t value);
    void writeWord(uint16_t addr, uint16_t value);
    bool writeRom(uint16_t addr, uint16_t value);
private:
    uint16_t *data;
    uint16_t ivtLimit;
    InvalidMemoryAddressException invalidMemAddrEx;
};

#endif // MEMORY_H
