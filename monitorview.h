#ifndef MONITORVIEW_H
#define MONITORVIEW_H

#include <QObject>
#include <QQuickImageProvider>
#include <emulator.h>

class MonitorView : public QObject, public QQuickImageProvider
{
    Q_OBJECT
public:
    explicit MonitorView(Emulator &emulator, QObject *parent = 0);
    ~MonitorView();
    QImage requestImage(const QString &id, QSize *size, const QSize &requestedSize) override;
    void fillRed();
public slots:
    void render();
private:
    QImage *m_image;
    Memory &m_memory;
};

#endif // MONITORVIEW_H
