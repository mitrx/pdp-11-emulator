#include "monitorview.h"

#include <QDebug>

#define DEFAULT_IMG_NAME "monitor.png"
#define DEFAULT_WIDTH 256
#define DEFAULT_HEIGHT 256

MonitorView::MonitorView(Emulator& emulator, QObject *parent)
    : QObject(parent), QQuickImageProvider(QQmlImageProviderBase::Image),
      m_memory(emulator.memory())
{
    m_image = new QImage(DEFAULT_WIDTH, DEFAULT_HEIGHT, QImage::Format_ARGB32);
}

void MonitorView::render()
{
    // TODO: Move pixel changing logic to this method
}

MonitorView::~MonitorView()
{
    delete m_image;
}

QImage MonitorView::requestImage(const QString &id, QSize *size, const QSize &requestedSize)
{
    int width;
    int height;

    // check what size should we use
    if (requestedSize.width() > 0 && requestedSize.height() > 0) {
        width = requestedSize.width();
        height = requestedSize.height();
    } else {
        width = DEFAULT_WIDTH;
        height = DEFAULT_HEIGHT;
    }
    // set the out parameter to chosen size
    *size = QSize(width, height);
    // if image is not initialized, then create one
    if (id == DEFAULT_IMG_NAME) {
        m_image->fill(QColor("white").rgba());
    }
    // otherwise just return current image
    return *m_image;
}

void MonitorView::fillRed()
{
    QColor blueColor("blue");
    for (int i = 0; i < 256; i++) {
        for (int j = 0; j < 256; j++) {
            QPoint pos(i, j);
            m_image->setPixelColor(pos, blueColor);
        }
    }
}
