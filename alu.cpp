#include "alu.h"

Alu::Alu(Registers& regs, Memory& mem)
    : m_registers(regs),
      m_memory(mem)
{
}
//
void Alu::setNZ(int16_t result)
{
    if (result < 0)
        m_registers.setN();
    else if (result == 0)
        m_registers.setZ();
}
void Alu::setNZb(int8_t result)
{
    if (result < 0)
        m_registers.setN();
    else if (result == 0)
        m_registers.setZ();
}
void Alu::pushStack(int16_t val)
{
    m_memory.writeRam(m_registers.sp(), val);
    m_registers.setSp(m_registers.sp() + 1);
}

int16_t Alu::popStack()
{
    int16_t val = m_memory.getWord(m_registers.sp());
    m_registers.setSp(m_registers.sp() - 1);
    return val;
}

void Alu::loadIvtPc(int16_t addr)
{
    m_registers.setPc(m_memory.ivtAddr(addr));
}
void Alu::loadIvtPs(int16_t addr)
{
    m_registers.setPsw(m_memory.ivtAddr(addr));
}

// Add Carry (word)
int16_t Alu::adc(int16_t x)
{
    int16_t res;
    if (m_registers.getC())
        res = x + 1;
    else
        res = x;
    setNZ(res);
    if ((x == MPI) && m_registers.getC())
        m_registers.setV();
    if ((x == -1) && m_registers.getC())
        m_registers.setC();
    return res;
}
// Add Carry (byte)
int8_t Alu::adcb(int8_t x)
{
    int8_t res;
    if (m_registers.getC())
        res = x + 1;
    else
        res = x;
    setNZb(res);
    if ((x == MPIB) && m_registers.getC())
        m_registers.setV();
    if ((x == -1) && m_registers.getC())
        m_registers.setC();
    return res;
}
// Add
int16_t Alu::add(int16_t x, int16_t y)
{
//    m_registers.setPsw();
    int16_t res = x + y;
    setNZ(res);
    // maybe need to add this if ((x < 0) && (y < 0) && (res > 0))
    if ((x > 0) && (y > 0) && (res < 0)) {
        m_registers.setV();
        m_registers.setC();
    }
    return res;
}
// Arithmetic shift
int16_t Alu::ash(int16_t x, int16_t y)
{
    int16_t res;
    if (y > 0)
        res = x << y;
    else if (y < 0)
        res = x >> -y;
    else
        res = x;
    setNZ(res);
    return res;
}
// Arithmetic shift combined
// x is the number of register
int32_t Alu::ashc(int16_t x, int16_t y, int16_t z)
{
    int32_t res = x;
    res <<= 16;
    res |= y;
    if (z > 0) {
        res >>= z;
    } else {
        res <<= -z;
    }
    return res;
}
// Arithmetic shift left
int16_t Alu::asl(int16_t x)
{
    int16_t res = x << 1;
    setNZ(res);
    if (x < 0)
        sec();
    if (m_registers.getC() == m_registers.getN())
        m_registers.clearV();
    else
        m_registers.setV();
    return res;
}
// Arithmetic shift left (byte)
int8_t Alu::aslb(int8_t x)
{
    int8_t res = x << 1;
    setNZb(res);
    if (x < 0)
        sec();
    if (m_registers.getC() == m_registers.getN())
        m_registers.clearV();
    else
        m_registers.setV();
    return res;
}
// Arithmetic shift right
int16_t Alu::asr(int16_t x)
{
    int16_t res = x >> 1;
    setNZ(res);
    if ((x & MASK(0)) == 0)
        clc();
    else
        sec();
    if (x < 0)
        sec();
    if (m_registers.getC() == m_registers.getN())
        m_registers.clearV();
    else
        m_registers.setV();
    return res;
}
// Arithmetic shift right byte
int8_t Alu::asrb(int8_t x)
{
    int8_t res = x >> 1;
    setNZb(res);
    if ((x & MASK(0)) == 0)
        clc();
    else
        sec();
    if (x < 0)
        sec();
    if (m_registers.getC() == m_registers.getN())
        m_registers.clearV();
    else
        m_registers.setV();
    return res;
}
// Clear
int16_t Alu::clr()
{
    m_registers.clearN();
    m_registers.setZ();
    m_registers.clearV();
    m_registers.clearC();
    return ((int16_t) 0);
}

int8_t Alu::clrb()
{
    m_registers.clearN();
    m_registers.setZ();
    m_registers.clearV();
    m_registers.clearC();
    return ((int8_t) 0);
}
// Compare
void Alu::cmp(int16_t x, int16_t y)
{
    int16_t res = x - y;
    setNZ(res);
    if ((x > 0) && (y < 0) && (res <= 0))
        m_registers.setV();
    if ((x < 0) && (y < 0) && (res > 0))
        m_registers.setV();
}
void Alu::cmpb(int8_t x, int8_t y)
{
    int8_t res = x - y;
    setNZb(res);
    if ((x > 0) && (y < 0) && (res <= 0))
        m_registers.setV();
    if ((x < 0) && (y < 0) && (res > 0))
        m_registers.setV();
}
// Complement
int16_t Alu::com(int16_t x)
{
    if ((~x) < 0)
        m_registers.setN();
    if ((~x) == 0)
        m_registers.setZ();
    m_registers.clearV();
    m_registers.setC();
    return ~x;
}
int8_t Alu::comb(int8_t x)
{
    if ((~x) < 0)
        m_registers.setN();
    if ((~x) == 0)
        m_registers.setZ();
    m_registers.clearV();
    m_registers.setC();
    return ~x;
}
// Decrement
int16_t Alu::dec(int16_t x)
{
    int16_t res = x - 1;
    setNZ(res);
    if (x == MNI)
        m_registers.setV();
    return res;
}
int8_t Alu::decb(int8_t x)
{
    int8_t res = x - 1;
    setNZb(res);
    if (x == MNIB)
        m_registers.setV();
    return res;
}
// Division
int32_t Alu::div(int16_t x, int16_t y, int16_t z)
{
    if (z == 0) {
        m_registers.setC();
        m_registers.setV();
    }
    int32_t fullNumber = x;
    fullNumber <<= 16;
    fullNumber |= y;
    if ((fullNumber < 0 && z > 0) || (fullNumber > 0 && z < 0)) {
        m_registers.setN();
    }
    if (fullNumber < 0) {
        fullNumber = -fullNumber;
    }
    if (z < 0) {
        z = -z;
    }
    if (fullNumber / z > UINT16_MAX || fullNumber % z > UINT16_MAX) {
        m_registers.setV();
    }
    return (((int16_t)(fullNumber / z)) << 16) | ((int16_t) fullNumber % z);
}

// Increment
int16_t Alu::inc(int16_t x)
{
    int16_t res = x + 1;
    setNZ(res);
    if (x == MPI)
        m_registers.setV();
    return res;
}
int8_t Alu::incb(int8_t x)
{
    int8_t res = x + 1;
    setNZb(res);
    if (x == MPIB)
        m_registers.setV();
    return res;
}
// Jump
void Alu::jmp(int16_t x)
{
    m_registers.setPc(x);
}
// Jump to subroutine
void Alu::jsr(int16_t reg, int16_t dst)
{
    int16_t tmp = dst;
    pushStack(reg);
    pushStack(m_registers.pc());
    m_registers.setPc(tmp);
}
// Move
int16_t Alu::mov(int16_t x, int16_t y)
{
    setNZ(x);
    m_registers.clearV();
    return y;
}
int8_t Alu::movb(int8_t x, int8_t y)
{
    setNZb(x);
    m_registers.clearV();
    return y;
}
// Multiply
int32_t Alu::mul(int16_t r, int16_t src)
{
    int32_t res = r * src;
    if (res < 0) {
        m_registers.setN();
    }
    if (res == 0) {
        m_registers.setZ();
    }
    m_registers.clearV();
    if ((res < INT16_MIN) || (res >= INT16_MAX)) {
        m_registers.setC();
    }
    if (res % 2 == 1) {
        res <<= 16;
    }
    return res;
}
// Negate
int16_t Alu::neg(int16_t x)
{
    setNZ((-x));
    if ((-x) == MNI)
        m_registers.setV();
    if ((-x) == 0)
        m_registers.clearC();
    else
        m_registers.setC();
    return -x;
}
int8_t Alu::negb(int8_t x)
{
    setNZb((-x));
    if ((-x) == MNIB)
        m_registers.setV();
    if ((-x) == 0)
        m_registers.clearC();
    else
        m_registers.setC();
    return (-x);
}
// Rotate left
int16_t Alu::rol(int16_t x)
{
    int16_t res = x << 1;
    setNZ(res);
    if (x < 0)
        m_registers.setC();
    else
        m_registers.clearC();
    if (m_registers.getC() == m_registers.getN())
        m_registers.clearV();
    else
        m_registers.setV();
    return res;
}
int8_t Alu::rolb(int8_t x)
{
    int8_t res = x << 1;
    setNZb(res);
    if (x < 0)
        m_registers.setC();
    else
        m_registers.clearC();
    if (m_registers.getC() == m_registers.getN())
        m_registers.clearV();
    else
        m_registers.setV();
    return res;
}
// Rotate right
int16_t Alu::ror(int16_t x)
{
    int16_t res = x >> 1;
    setNZ(res);
    if ((x & MASK(0)) == 0)
        m_registers.clearC();
    else
        m_registers.setC();
    if (m_registers.getC() == m_registers.getN())
        m_registers.clearV();
    else
        m_registers.setV();
    return res;
}
int8_t Alu::rorb(int8_t x)
{
    int8_t res = x >> 1;
    setNZb(res);
    if ((x & MASK(0)) == 0)
        m_registers.clearC();
    else
        m_registers.setC();
    if (m_registers.getC() == m_registers.getN())
        m_registers.clearV();
    else
        m_registers.setV();
    return res;
}
// Subtract Carry
int16_t Alu::sbc(int16_t x)
{
    int16_t res;
    if (m_registers.getC())
        res = x - 1;
    else
        res = x;
    setNZ(res);
    if (x == MNI)
        m_registers.setV();
    if ((x == 0) && m_registers.getC())
        m_registers.setC();
    return res;
}
int8_t Alu::sbcb(int8_t x)
{
    int8_t res;
    if (m_registers.getC())
        res = x - 1;
    else
        res = x;
    setNZb(res);
    if (x == MNIB)
        m_registers.setV();
    if ((x == 0) && m_registers.getC())
        m_registers.setC();
    return res;
}
// Subtract
int16_t Alu::sub(int16_t x, int16_t y)
{
    int16_t res = y - x;
    setNZ(res);
    if ((y > 0) && (x < 0) && (res < 0))
        m_registers.setV();
    if ((y < 0) && (x > 0) && (res > 0))
        m_registers.setV();
    if ((y + (~x) + 1) < 65536)
        m_registers.setC();
    return res;
}
// Swap byte
int16_t Alu::swab(int16_t x)
{
    int16_t opc_low_byte = (int16_t) 0b0000000011111111;
    int16_t opc_high_byte = (int16_t) 0b1111111100000000;
    int16_t res = (x & opc_low_byte) << 8;
    res = res | (((x & opc_high_byte) >> 8) & opc_high_byte);
    m_registers.clearV();
    m_registers.clearC();
    if ((res & MASK(7)) != 0)
        m_registers.setZ();
    if ((res & opc_low_byte) == 0)
        m_registers.setZ();
    return res;
}
// Sign Extend
int16_t Alu::sxt()
{
    m_registers.clearV();
    if (m_registers.getN())
        return -1;
    else {
        m_registers.setZ();
        return 0;
    }
}
// Exclusive OR
int16_t Alu::exor(int16_t x, int16_t y)
{
    int16_t res = x ^ y;
    setNZ(res);
    m_registers.clearV();
    return res;
}
// bit clear
int16_t Alu::bic(int16_t x, int16_t y)
{
    int16_t res = (~x) & y;
    setNZ(res);
    clv();
    return res;
}
int8_t Alu::bicb(int8_t x, int8_t y)
{
    int8_t res = (~x) & y;
    setNZb(res);
    clv();
    return res;
}
// bit set
int16_t Alu::bis(int16_t x, int16_t y)
{
   setNZ((x | y));
   clv();
   return (x | y);
}
int8_t Alu::bisb(int8_t x, int8_t y)
{
   setNZb((x | y));
   clv();
   return (x | y);
}
// bit test
int16_t Alu::bit(int16_t x, int16_t y)
{
    setNZ((x & y));
    clv();
    return (x & y);
}
int8_t Alu::bitb(int8_t x, int8_t y)
{
    setNZb((x & y));
    clv();
    return (x & y);
}

// Condition codes
void Alu::ccc()
{
    m_registers.clearN();
    m_registers.clearZ();
    m_registers.clearV();
    m_registers.clearC();
}
void Alu::clc()
{
    m_registers.clearC();
}
void Alu::cln()
{
    m_registers.clearN();
}
void Alu::clv()
{
    m_registers.clearV();
}
void Alu::clz()
{
    m_registers.clearZ();
}
void Alu::scc()
{
    m_registers.setN();
    m_registers.setZ();
    m_registers.setV();
    m_registers.setC();
}
void Alu::sec()
{
    m_registers.setC();
}
void Alu::sen()
{
    m_registers.setN();
}
void Alu::sev()
{
    m_registers.setV();
}
void Alu::sez()
{
    m_registers.setZ();
}

// if carry clear
void Alu::bcc(int16_t offset)
{
    if (!m_registers.getC())
        m_registers.setPc(2 * offset + m_registers.pc());
}
// if carry set
void Alu::bcs(int16_t offset)
{
    if (m_registers.getC())
        m_registers.setPc(2 * offset + m_registers.pc());
}
// if zero
void Alu::beq(int16_t offset)
{
    if (m_registers.getZ())
       m_registers.setPc(2 * offset + m_registers.pc());
}
// if greater than or equal
void Alu::bge(int16_t offset)
{
    if (m_registers.getN() == m_registers.getV())
        m_registers.setPc(2 * offset + m_registers.pc());
}
// if greater than
void Alu::bgt(int16_t offset)
{
    if (!((m_registers.getN() == m_registers.getV()) && m_registers.getZ()))
        m_registers.setPc(2 * offset + m_registers.pc());
}
// if higher
void Alu::bhi(int16_t offset)
{
    if ((m_registers.getC() == false) && (m_registers.getZ() == false))
        m_registers.setPc(2 * offset + m_registers.pc());
}
// if less than or equal to
void Alu::ble(int16_t offset)
{
    if ((m_registers.getN() == m_registers.getV()) && m_registers.getZ()) {
        m_registers.setPc(2 * offset + m_registers.pc());
    }
}
// if lower or same
void Alu::blos(int16_t offset)
{
    if (m_registers.getC() || m_registers.getZ()) {
        m_registers.setPc(2 * offset + m_registers.pc());
    }
}
// if less than
void Alu::blt(int16_t offset)
{
    if (m_registers.getN() != m_registers.getV()) {
        m_registers.setPc(2 * offset + m_registers.pc());
    }
}
// if minus
void Alu::bmi(int16_t offset)
{
    if (m_registers.getN() == true) {
        m_registers.setPc(2 * offset + m_registers.pc());
    }
}
// if not equal
void Alu::bne(int16_t offset)
{
    if (!m_registers.getZ()) {
        m_registers.setPc(2 * offset + m_registers.pc());
    }
}
// if plus
void Alu::bpl(int16_t offset)
{
    if (!m_registers.getN()) {
       m_registers.setPc(2 * offset + m_registers.pc());
    }
}
// unconditional branch
void Alu::br(int16_t offset)
{
    m_registers.setPc(m_registers.pc() + offset * 2);
}
// V bit clear branch
void Alu::bvc(int16_t offset)
{
    if (!m_registers.getV()) {
        m_registers.setPc(m_registers.pc() + offset * 2);
    }
}
// V bit set branch
void Alu::bvs(int16_t offset)
{
    if (m_registers.getV()) {
        m_registers.setPc(m_registers.pc() + offset * 2);
    }
}
// Emulator trap
void Alu::emt()
{
    pushStack(m_registers.psw());
    pushStack(m_registers.pc());
    loadIvtPc((int16_t)030);
    loadIvtPs((int16_t)032);
}

void Alu::iot()
{
    pushStack(m_registers.psw());
    pushStack(m_registers.pc());
    loadIvtPc((int16_t)020);
    loadIvtPs((int16_t)022);
}
// breakpoint
void Alu::bpt()
{
    pushStack(m_registers.psw());
    pushStack(m_registers.pc());
    loadIvtPc((int16_t)014);
    loadIvtPs((int16_t)016);
}
// return from interrupt
void Alu::rti()
{
    m_registers.setPc(popStack());
    m_registers.setPsw(popStack());
}
// return from subroutine
int16_t Alu::rts(int16_t reg)
{
    m_registers.setPc(reg);
    return popStack();
}
// return from interrupt
void Alu::rtt()
{
    m_registers.setPc(popStack());
    m_registers.setPsw(popStack());
}
// trap
void Alu::trap()
{
    pushStack(m_registers.pc());
    pushStack(m_registers.psw());
    loadIvtPc((int16_t)034);
    loadIvtPs((int16_t)036);
}
// mark
void Alu::mark(int16_t N)
{
    m_registers.setSp(m_registers.pc() + 2 * N);
    m_registers.setPc(m_registers.r5());
    m_registers.setR5(popStack());
}
// move byte from PSW
int16_t Alu::mfps()
{
    int16_t opc_low_byte = (int16_t) 0b0000000011111111;
    int16_t res = m_registers.psw() & opc_low_byte;
    if ((res & MASK(7)) != 0) {
        m_registers.setN();
    }
    if (res == 0) {
        m_registers.setZ();
    }
    m_registers.clearV();
    return res;
}
// move from previouse
void Alu::mfpd(int16_t src)
{
    setNZ(src);
    m_registers.clearV();
    pushStack(src);
}
// move to previouse data space
int16_t Alu::mtpd()
{
    int16_t res = popStack();
    setNZ(res);
    m_registers.clearV();
    return res;
}
// move byte to PSW
void Alu::mtps(int16_t src)
{
    int16_t opc_low_byte = (int16_t) 0b0000000011111111;
    int16_t res = src & opc_low_byte;
    if (src == 0)
        m_registers.setZ();
    m_registers.clearV();
    m_registers.setPsw(res);
}




















