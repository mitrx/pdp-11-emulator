#ifndef EMULATORCONTROLLER_H
#define EMULATORCONTROLLER_H

#include <QObject>
#include "emulator.h"

class EmulatorController : public QObject
{
    Q_OBJECT
public:
    explicit EmulatorController(Emulator& emulator, QObject *parent = 0);
    Q_INVOKABLE void initialize();
    Q_INVOKABLE void run();
    Q_INVOKABLE void step();
    Q_INVOKABLE void reset();
    Emulator& emulator();
private:
    Emulator& m_emulator;
};

#endif // EMULATORCONTROLLER_H
